#!/usr/bin/python

"""Rename account's login name.

Rename account name to a new one. The old username is retained as 
a nickname to ensure continuous mail delivery in the case of email 
forwarding settings and will not be available as a new username. 
Use --delete option to delete the old account name. 

This process may take upto 10 minutes to populate all Google services.  

Usage: rename_user [options]

Options:
        -u / --username=[username]
                Username to be renamed.
               
        -n / --newusername=[newusername]
                New account name.
               
        -d / --delete 
                Delete the old username
               
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit, parse_email
from lib.Directory import *
from lib.SuService import SuUsers

def short_desc():
    return "Rename a user account to a new name."
    
def main(*args):
    """Use Provisionning API to update account's information."""
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hdu:n:', ['help', 'delete','username=','newusername='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = ''
    newusername = ''
    delete = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username,domain = parse_email(arg)
        elif option in ('-n', '--newusername'):
            newusername,newdomain = parse_email(arg)
        elif option in ('-d', '--delete'):
            delete = True
    
    su_service = SuUsers()
    if delete:
        if username and not newusername:
            su_service.DeleteNickname(username)
            sys.exit(0)
         
    if not (username and newusername):
        msg = "username and newusername are required."
        sys_exit(1,msg)

    if username == newusername:
        msg = "Old username and newusername cannot be same."
        sys_exit(1,'old username and new username cannot be same')

    if domain != newdomain:
        sys_exit(1,'newusername has to be in the same domain as the old username')

    if not is_valid_id(newusername):
        sys_exit(1,'new username is not a valid sunetid')

    su_service = SuUsers()
    user_entry = su_service.RetrieveUser(username)
    
    user_entry.login.user_name = newusername
    su_service.UpdateUser(username,user_entry)
    
    if delete:
        su_service.DeleteNickname(username)

if __name__ == '__main__':
    main(*sys.argv[1:])
