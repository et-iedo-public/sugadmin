#!/usr/bin/env python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""Create a new user account.

This script creates a new user account in Google Apps domain. The account name 
is the sunetid. Display name and nicknames are also created from the data in 
Stanford's Directory.

If a file containing a list of sunetids is given at the command line, and no 
initial password is given at the command line, these accounts will be created 
with a random password.  The SHA1 encrypted password will be sent to Google.

Options:
        -u / --username=[username]
            Primary account name to be created.
        -f / --file=[file]
            File with list of user's sunetids to be created.
        -p / --password=[password]
            Initial password. 
        -c / --classofservice=[class of service]
            Class of service.
            cos1: Full services enabled (primarily for GSB)
            cos2: Default. No email and calendar (staff/faculty)
            cos3: Full services enabled (undergraduate students)
        -h / --help
            Print this message and exit
                
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import time
import hashlib

import paths
import gdata_cfg
from gdata.service import DEFAULT_NUM_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF, RanOutOfTries
from lib.Utils import sys_exit
from lib.Utils import parse_email,readlines,generate_password,add_domain
from lib.Directory import is_valid_id, is_active, directory_query
from lib.SuService import SuUsers, SuOrganization
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Create a new user account with an initial password."

def create_account(user,password):
    """Create user account
    """

    attributes = [gdata_cfg.LASTNAME_FIRSTNAME]
    filter = 'uid=' + user
    name_lf = directory_query(filter,attributes,gdata_cfg.SEARCH_PATH_PPL)
    # Need to handle people just have one name, multiple names 
    all_names=name_lf[0].split(',')
    if len(all_names) == 1:
        familyname = givenname = all_names[0] 
    else:
        familyname,givenname=all_names[0].strip(),all_names[1].strip()
    
    su_service = SuUsers()
    
    # Retry if we got 503 service unavailable due to ratelimit
    mtries, mdelay = DEFAULT_NUM_RETRIES, DEFAULT_DELAY
    while mtries > 0:
        try:
            su_service.CreateUser(user,familyname,givenname,password, password_hash_function="SHA-1")
            print "%s is created." % (user)
            break
        except KeyboardInterrupt:
            raise
        except AppsForYourDomainException, e:
            if e.args[0]['status'] == 503:
                mtries -= 1
                mdelay *= DEFAULT_BACKOFF
                time.sleep(mdelay)
            else:
                raise AppsForYourDomainException, e

    if mtries == 0:
        raise RanOutOfTries('Ran out of tries.')
   
    attributes = ['suSunetID']
    filter = 'uid=' + user
    nicknames = directory_query(filter,attributes,gdata_cfg.SEARCH_PATH_PPL)
    delay = 0
    for nickname in nicknames:
        if nickname != user:
            mtries, mdelay = DEFAULT_NUM_RETRIES, DEFAULT_DELAY
            while mtries > 0:
                try:
                    su_service.CreateNickname(user,nickname)
                    print "nickname %s is created for %s." % (nickname,user)
                    break
                except AppsForYourDomainException, e:
                    if e.args[0]['status'] == 503:
                        mtries -= 1
                        mdelay *= DEFAULT_BACKOFF
                        time.sleep(mdelay)
                    else:
                        raise AppsForYourDomainException, e
            if mtries == 0:
                raise RanOutOfTries('Ran out of tries.')

def move_to_cos(user,cos):
    """Move a user to another class of service group
    """

    user = add_domain(user)
    su_service = SuOrganization()
    customerid = su_service.get_customer_id()
    mtries, mdelay = DEFAULT_NUM_RETRIES, DEFAULT_DELAY
    while mtries > 0:
        try:
            su_service.UpdateOrgUser(customerid,user,cos)
            print "%s is updated to %s." % (user, cos)
            break
        except AppsForYourDomainException, e:
            if e.args[0]['status'] == 503 or e.args[0]['status'] == 400:
                mtries -= 1
                mdelay *= DEFAULT_BACKOFF
                time.sleep(mdelay)
            else:
                raise AppsForYourDomainException, e
    if mtries == 0:
        raise RanOutOfTries('Ran out of tries.')
   
def main(*args):
    """ Create user accounts
    """
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:p:f:c:', ['help', 'username=', 'password=', 'file=','classofservice='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = file = password = '' 
    cos = 'cos2'
    genpassword = True

    # Process options
    for option, arg in opts:
        if option in ('-u', '--username'):
            username,domain=parse_email(arg)
        elif option in ('-p','--password'):
            password = hashlib.sha1(arg).hexdigest()
            genpassword = False
        elif option in ('-f', '--file'):
            file = arg
        elif option in ('-c', '--classofservice'):
            cos = arg
        elif option in ('-h', '--help'):
            sys_exit(0,__doc__)

    if username and file:
        msg = "Use username or file, not both."
        sys_exit(1,msg)
        
    if not username and not file:
        msg = "username or file is required."
        sys_exit(1,msg)
        
    #if (username and not password):
    #    msg = "Initial password is required.\n"
    #    sys_exit(1, msg )

    if cos and cos not in ('cos1', 'cos2', 'cos3'):
        msg = "Wrong class of service.\n"
        sys_exit(1, msg )
    else:
        cos = gdata_cfg.CLASSOFSERVICES[cos]
    
    accounts = []
    rc = 0
    if username:
        accounts.append(username)
    else:
        lines = readlines(file)
        for l in lines:
            accounts.append(l.rstrip())
        
    for user in accounts:
        if not is_valid_id(user):
            print "%s is not a valid sunetid." % (user)
            rc = 1
            continue
        elif not is_active(user):
            print "%s is not an active account." % (user)
            rc = 1
            continue

        if genpassword:
            password = generate_password()
    
        try:
            create_account(user,password)
            time.sleep(3)
            move_to_cos(user,cos)
        except AppsForYourDomainException, e:
            if e.args[0]['body'].rfind('EntityExists') > 0:
                print "ERROR in creating account: %s exists" % (user)
            elif e.args[0]['body'].rfind('EntityDoesNotExist') > 0:
                print "ERROR in move-to-cos account: %s does not exist" % (user)
            else:
                print e
            rc = 1
        except RanOutOfTries, e:
            print e
            rc = 1

    sys.exit(rc)
    
# Only execute the code when it's called as a script, not just imported.
if __name__ == '__main__':
    main(*sys.argv[1:])
