#!/usr/bin/python
#
# Copyright 2012
#         The Board of Trustees of the Leland Stanford Junior University

"""create_resource: create a Google Apps calendar resource

Usage: create-resource options

Options:
        -r / --resource=[resourceid]
                Required. resourceid to create.
                A resource name shall begin with the following prefix:
                cal_lo_building_room_ ... for location
                cal_eq_<name>_ .... for equipment
                cal_sc_... for shared resources, such as shared calendars
        -n / --name=[resource display name]
                Optional. A short name for the resource to be created. Default is generated from resourceID.
                This is the name that will show up when users schedule event with the resource.
        -c / --capacity=[number]
                Optional. Capacity of the room. Use this option to show the capacity with the display name.
        -t / --telephone=[number]
                Optional. Phone number of the room.
        -e / --equipment
               Optional. Equipments.
        -d / --description=[Description and information about the resource ]
                Optional. Default is the resource name.
        -z / --zimbra
                Optional. Use Zimbra data as input. When -z is present, other data except resource id is ignored.
        -q / --quiet 
                Do not show result of the creation. Default is to show the result.
        -h / --help
                Print this message and exit
    
Create a Google Apps calendar resource in primary domain.

Example: 
    ceate_resource --resource cal_lo_polyan_all_152 --zimbra
        This will create a resource with information from Zimbra.
        
    create_resource --resource cal_lo_polya_hall_152 --capacity 16 --telephone 123-456-7890 --equipment projector
        This will create a location type resource, with a display name "Polya Hall-152 (16,123-456-7890,projector)".

"""

__author__ = 'sfeng@stanford.edu'

import os
import sys
import getopt
import time
import remctl
import json
import ConfigParser

import paths
import gdata_cfg
import gdata.calendar
from gdata.apps.service import AppsForYourDomainException
from lib.Utils import sys_exit, remove_domain, add_domain
from lib.SuService import SuResource, SuCalendar

# Global configurations
hostname = os.uname()[1] 
config = ConfigParser.SafeConfigParser()
config.read('/etc/gadmin/maildrop')
defaults = config.defaults()
KRB5CCNAME = defaults['krb5ccname']
ACL_URI_BASE = '/calendar/feeds/%s/acl/full'

def short_desc():
    return "Create a Google Apps calendar resource."

def get_from_zimbra(calendar):
    """Create resource with information from Zimbra
    """
    cmd = ('cal-admin', 'show-for-google', calendar)
    os.environ['KRB5CCNAME'] = KRB5CCNAME
    try:
        result = remctl.remctl(host='zm09.stanford.edu',command=cmd)

        if result.stderr:
            sys.exit(result.stderr)
        elif result.stdout:
            return json.loads(result.stdout)
        else:
            sys.exit(calendar + " has no zimbra data!")

    except remctl.RemctlProtocolError, error:
        print "Resouce does not exist."
        sys.exit(error)

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqzr:n:c:t:e:d:', \
            ['help','zimbra','quiet','resourceid=', 'name=', 'capacity=', 'telephone=', 'equipment=', 'description='])
    except getopt.error, msg:
        sys_exit(1,msg)

    resourceId = name = description = capacity = telephone= equipment = ''
    zimbra = quiet = False
    
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-r', '--resourceid'):
            resourceId = arg.lower()
        elif option in ('-n', '--name'):
            resourceCommonName = arg
        elif option in ('-c', '--capacity'):
            capacity = arg
        elif option in ('-t', '--telephone'):
            telephone = arg
        elif option in ('-z', '--zimbra'):
            zimbra = True
        elif option in ('-e', '--equipment'):
            equipment = arg
        elif option in ('-d', '--description'):
            description = arg
        if option in ('-q', '--quiet'):
            quiet = True

    if not resourceId:
        msg = "Resource id is required."
        sys_exit(1,msg)
    else:
        prefix = resourceId[:7] 
        try:
            resourceType = gdata_cfg.RESOURCE_MAP[prefix]
        except KeyError:
            print "Wrong resource type. Should be one of the following:"
            print ', '.join(gdata_cfg.RESOURCE_MAP.keys())
            sys.exit(1)
        resourceType = gdata_cfg.RESOURCE_MAP[prefix]
    
    if zimbra:
        data = get_from_zimbra(resourceId)
        capacity = data['resourceCapacity']
        resourceType = data['type']
        description = data['description']
        if description.find('ph:') >= 0:
            telephone = description[4:].split(';')[0]
        resourceCommonName = data['displayName']

        # Standardize admin email address
        resourceOwner = data['contactEmail']
        resourceOwner = remove_domain(resourceOwner)
        resourceOwner = add_domain(resourceOwner)
        
    extra = ''
    if capacity:
        try:
            capacity=int(capacity)
        except ValueError:
            print "capacity should be a number."
            sys.exit(1)
        else:
            if capacity < 1:
                print "Capacity should be larger than 1."
                sys.exit(1)
        
    if not name:
        # Generate name e.g: cal_lo_polya_hall_152 become Polay Hall-152
        name = resourceId[7:].title().replace('_',' ')
        last_space = name.rfind(' ')
        building = name[:last_space]
        room = name[last_space+1:]
        resourceCommonName = "%s-%s" % (building, room)
        
        
    if not description:
        description = resourceCommonName.replace('-', ' ')
    
    if capacity:
        extra = "%s"  % (capacity)
    if telephone:
        if extra:
            extra = "%s, %s"  % (extra, telephone)
        else:
            extra = "%s"  % (telephone)
        
    if equipment:
        if extra:
            extra = "%s, %s"  % (extra, equipment)
        else:
            extra = "%s"  % (equipment) 
           
    if extra:  
        resourceCommonName = "%s (%s)" % (resourceCommonName,extra)
        
    if not description:
        description = resourceCommonName.replace('-', ' ')
           
    su_service = SuResource()
    try:
        new_resource = su_service.CreateResourceCalendar(resourceId, resourceCommonName,description,resourceType)
        time.sleep(2) 
    except AppsForYourDomainException, e:
        if e.args[0]['body'].rfind('EntityExists') > 0:
            sys.exit("Resource already exists")

    if new_resource and resourceOwner:
        r = remove_domain(resourceId)
        resource_entry = SuResource().RetrieveResourceCalendar(r)
        cal = resource_entry['resourceEmail']
        print "CA" + cal
        SuCalendar().grant_acl(cal, resourceOwner, 'owner')
        
    if not quiet:
        time.sleep(1)
        resource = su_service.RetrieveResourceCalendar(resourceId)
        for k,v in resource.iteritems():
            print "%-30s: %s" % (k, v)

if __name__ == '__main__':
    main(*sys.argv[1:])
