#!/usr/bin/python

"""Get primary id for a given nickname.

Given a 'nickname', a.k.a. sunetid alias, this script displays its the primary 
owner's id. To get a primary account's all nicknames, use 'get-user' command.

Usage: get_nickname [options]

Options:
        -n / --nickname=[nickname | ALL]
                Display the primary account name for the nickname, or retrieve
all nicknames in the domain.
        -h / --help
                Print this message and exit

Example: get_nickname -n Firstname.LastName

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email
from lib.Directory import directory_query
from lib.SuService import SuUsers

def short_desc():
    return "Get primary account name for a given nickname."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hn:', ['help', 'nickname='])
    except getopt.error, msg:
        sys_exit(1, msg)

    nickname = '' 

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-n','--nickname'):
            nickname,domain = parse_email(arg)

    if not nickname:
        msg = "nickname is required."
        sys_exit(1,msg)

    su_service = SuUsers()
    if nickname == 'ALL':
        all = []
        print "Getting all nicknames in the domain, may take a while..."
        sys.stdout.flush()
        for feed in su_service.GetGeneratorForAllNicknames():
            for entry in feed.entry:
                all.append(entry.login.user_name + ',' + entry.nickname.name)
            all.sort()
            for a in all:
                print a
    elif SuUsers().is_ga_user(nickname):
        msg = nickname + " is a primary account name."
        sys_exit(1,msg)
    else:
        entry = su_service.RetrieveNickname(nickname)
        print "%s belongs to %s." % (nickname, entry.login.user_name)

if __name__ == '__main__':
    main(*sys.argv[1:])
