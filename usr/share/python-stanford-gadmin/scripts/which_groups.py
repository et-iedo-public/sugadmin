#!/usr/bin/python

# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Get all groups that the user is a member of."    

Given a user's email address, list all groups that the user belongs to.

Usage: which_groups [options]

Options:
        -m / --memberaddr=[member email]
        -h / --help
                Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator
import time

import paths
from lib.Utils import sys_exit
import gdata_cfg
from lib.SuService import SuGroups

def short_desc():
    return "Get all groups that the user is a member of."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hm:', ['help','memberaddr='])
    except getopt.error, msg:
        sys_exit(1,msg)

    memberaddr = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-m', '--memberaddr'):
            memberaddr = arg

    if not memberaddr:
        msg = "memberaddr is required."
        sys_exit(1,msg)

    su_service = SuGroups()
    all_lists_feed = su_service.RetrieveGroups(memberaddr)
    all_lists_feed.sort(key=operator.itemgetter('groupId'))
    for list in all_lists_feed:
        print list['groupId']
        
if __name__ == '__main__':
    main(*sys.argv[1:])
