#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Get login information about a user

Usage: get_user [options]

Options:
        -u / --email=[username]
        -q / --quiet 
           Quiet mode. No output. Use it to check if a user exists.
        -h / --help
           Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

from lib.Utils import sys_exit, parse_email, add_domain
from lib.SuService import SuUsers, SuOrganization
import gdata_cfg

def short_desc():
    return "Get a user's account information."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqu:', ['help', 'quiet','username='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = ''
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u', '--username'):
            username,domain=parse_email(arg)
        elif option in ('-q', '--quiet'):
            quiet = True

    if not username:
        msg = 'username is required.'
        sys_exit(1,msg)

    su_service = SuUsers()
    user_entry = su_service.RetrieveUser(username)
    user_nickname = su_service.RetrieveNicknames(username)
    nicknames = []
    for entry in user_nickname.entry:
        nicknames.append(entry.nickname.name)
    su_service= SuOrganization()
    useremail = add_domain(username)
    user_org = su_service.get_user_cos(useremail)

    if not quiet:
        print "%-30s: %s" % ('Username', add_domain(user_entry.login.user_name))
        print "%-30s: %s" % ('Family name', user_entry.name.family_name)
        print "%-30s: %s" % ('Given name', user_entry.name.given_name)
        print "%-30s: %s" % ('Nicknames', ' '.join(nicknames))
        print "%-30s: %s" % ('Account Suspended:', user_entry.login.suspended)
        print "%-30s: %s" % ('Is Admin', user_entry.login.admin)
        print "%-30s: %s" % ('Agreed To Terms:',user_entry.login.agreed_to_terms )
        print "%-30s: %s" % ('Class of Service:', user_org['orgUnitPath'])

    sys_exit(0,'')

if __name__ == '__main__':
    main(*sys.argv[1:])
