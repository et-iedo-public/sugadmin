#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""
    delete_email_delegate: delete a Google Apps Email delegate

Usage: delete_emmail_delegate [options] 

Options:
        -u / --username=[username]
                Required. The delegator's username.
        -d / --delegate=[username]
                Required. The delegate's username.
        -q / --quiet
                quiet mode. No output. Default is False.
        -h / --help
                Print this message and exit.
    
Delete a Google Apps email delegate.

Example: delete_email_delegate -u liz -d tom
    Delete delegate "tom" from "liz"'s account.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email, add_domain
from lib.SuService import SuEmail
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Delete a Google Apps Email delegate."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqu:d:', ['help', 'quiet','username=', 'delegate='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = delegate = ''
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u', '--username'):
            username,udomain = parse_email(arg)
        elif option in ('-d', '--delegate'):
            delegate,ddomain = parse_email(arg)
        elif option in ('-q', '--quiet'):
            quiet = True

    if not (username and delegate):
        msg = "Missing username or delegate.\n"
        sys_exit(1, msg)
        
    if udomain and udomain.lower() != gdata_cfg.DOMAIN:
        msg = "delegator has to be in "+  gdata_cfg.DOMAIN + " domain."
        sys_exit(1,msg)

    if ddomain and ddomain.lower() != gdata_cfg.DOMAIN:
        msg = "delegate has to be in "+  gdata_cfg.DOMAIN + " domain."
        sys_exit(1,msg)

    su_service = SuEmail()
    
    delegate = add_domain(delegate)

    su_service.DeleteEmailDelegate(delegator=username, delegate=delegate)

    if not quiet:
        print "Deleted delegate %s from delegator %s." %(delegate, username)
        
if __name__ == '__main__':
    main(*sys.argv[1:])
