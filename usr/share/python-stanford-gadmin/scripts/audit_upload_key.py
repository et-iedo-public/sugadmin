#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University


"""
    audit-upload-key: 

Usage: audit_upload_key [options]

Options:
        -f / --file key file for uploading domain's public encryption key
        -v / --verbose
        -h / --help
                Print this message and exit
                
This script uploads public key to Google. The key is used to encrypt maillbox downloaded from Google.
System administrators need to decrypt with the private key generated with the public key.

"""
__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit 
from lib.Utils import readfile
from lib.SuService import SuAudit
import gdata_cfg

def short_desc():
        return "Upload domain's public encryption key."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hvf:', ['help','verbose','file='])
    except getopt.error, msg:
        sys_exit(1,msg)

    file = None
    verbose = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-f', '--file'):
            file = arg
        elif option in ('-v', '--verbose'):
            verbose = True

    if not file:
        msg = "File to contain public key is required." 
        sys_exit(1,msg)
    else:
        key = readfile(file)
            
    su_service = SuAudit()
    result = su_service.updatePGPKey(key)
    if verbose:
            for k,v in result.iteritems():
                    print k, ": ", v 

if __name__ == '__main__':
    main(*sys.argv[1:])
