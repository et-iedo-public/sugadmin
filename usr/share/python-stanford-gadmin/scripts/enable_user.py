#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Enable an account from suspended mode.

Usage: enable_user [options]
Options:
        -u / --username=[username]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import getopt
import sys

import paths
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuUsers

def short_desc():
    return "Enable an account from suspended mode."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:', ['--help', 'username='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u', '--username'):
            username,domain = parse_email(arg)

    if not username:
        msg = "username is required."
        sys_exit(1, msg)

    su_service = SuUsers()
    su_service.RestoreUser(username)

if __name__ == '__main__':
    main(*sys.argv[1:])
