#!/usr/bin/python

"""Get all groups in the domain account.

Get all groups in the domain, including subdomains. The GA code doesn't 
seem to support subdomain queries.

Usage: get_all_groups [options]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator
import pprint

import paths
import gdata_cfg
from lib.Utils import sys_exit
from lib.SuService import SuGroups

def short_desc():
    return "Get all groups in the domain."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'h')
    except getopt.error, msg:
        sys_exit(1,msg)

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)

    su_service = SuGroups()
    all_groups = su_service.RetrieveAllGroups()
    all_groups.sort(key=operator.itemgetter('groupId'))
    for su_service in all_groups:
        print su_service['groupId'], su_service['groupName']

if __name__ == '__main__':
    main(*sys.argv[1:])
