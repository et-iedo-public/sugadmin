#!/usr/bin/python
#
# Copyright 2011, 2012
#     The Board of Trustees of the Leland Stanford Junior University
#


"""Remove one or more users or another group from an existing Google Apps group

Usage: remove_members [options] <memberaddr1> <memberaddr2> ...

Options:
        -g / --groupaddr=[groupaddr]
        -r / --remove_all_suspended
        -h / --help
                Print this message and exit

     member1 member2

You can either choose to remove selected members or remove all suspended 
members, but not both.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt
import time
import operator

from lib.Utils import sys_exit, parse_email
from lib.SuService import SuGroups

def short_desc():
    return "Remove user(s) or group(s) from a Google Apps groups."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqrg:', ['help', 'quiet', 
                     'remove_all_suspended', 'groupaddd='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupaddr = ''
    remove_all_suspended = False
    quiet = False
    members = [x.lower() for x in args]

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupaddr'):
            groupaddr = arg
        elif option in ('-r', '--remove_all_suspended'):
            remove_all_suspended = True
        elif option in ('-q', '--quiet'):
            quiet = True

    if not groupaddr: 
        msg = " GroupId is required."
        sys_exit(1,msg)

    if remove_all_suspended and members:
        msg = "remove_all_suspended, or remove selected members, not both."
        sys_exit(1,msg)

    if not ( remove_all_suspended or members ):
        sys_exit(1,__doc__)

    su_service = SuGroups()
    # Callback object to get memberId
    getid = operator.itemgetter('memberId')

    # Retrive all group members, including suspeneded
    all_member_list = su_service.RetrieveAllMembers(groupaddr,True)
    all_member_ids = list(map(getid, all_member_list))
    
    # Retrive all active group members
    active_member_list = su_service.RetrieveAllMembers(groupaddr,False)
    active_member_ids = list(map(getid, active_member_list))

    if remove_all_suspended:
        for member in all_member_ids:
            if member not in active_member_ids:
                result = su_service.RemoveMemberFromGroup(member,groupaddr)
                print member + ' is removed from ' + groupaddr
    else:
        for m in members:
            if m not in all_member_ids:
                print m + ' is not a member of ' + groupaddr
                if m.find('@') <= 0:
                    print "Try to use fully qualified email address." 
            else:
                result = su_service.RemoveMemberFromGroup(m,groupaddr)
                print m + ' is removed from ' + groupaddr
            time.sleep(1) 

if __name__ == '__main__':
    main(*sys.argv[1:])
