#!/usr/bin/python
#
# Copyright 2012
#         The Board of Trustees of the Leland Stanford Junior University

"""get_resource: Get a Google Apps calendar resource information

Usage: get_resource --resource "resourceid" | ALL

Options:
        -r / --resource=[resourceid]
                Required. 
                A resource name shall begin with the following prefix:
                cal_lo_building_room_ ... for location
                cal_eq_<name>_ .... for equipment
                cal_sc_... for shared resources, such as shared calendars
                If resourceid is 'ALL' get all resourcesid in the domain.
        -f / --force
                Do not check naming convention. Needed for resources created through other ways.
        -s/ --search
                Find a resource that contains the keyword in the resoruceId. 
        -h / --help
                Print this message and exit
    
Get a Google Apps calendar resource information. 

Example: 
    get_resource --resource cal_lo_polya_hall_152 
    get_resource --search polya
    get_resource --resource ALL
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator

import paths
import gdata_cfg
from lib.Utils import sys_exit, remove_domain
from lib.SuService import SuResource
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Get a Google Apps calendar resource information."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hfsr:', ['help','force','search','resourceid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    resourceId = ''
    force = search = False
    
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-r', '--resourceid'):
            # resourceId has no domain value
            resourceId = remove_domain(arg.lower())
        elif option in ('-f','--force'):
            force= True
        elif option in ('-s','--search'):
            search = True

    if not resourceId:
        msg = "Resource Id or a keyword is required."
        sys_exit(1,msg)
    elif resourceId not in ('all') and not force and not search:
        prefix = resourceId[:7] 
        try:
            gdata_cfg.RESOURCE_MAP[prefix]
        except KeyError:
            print "Wrong resource type. Should be one of the following:"
            print ', '.join(gdata_cfg.RESOURCE_MAP.keys())
            sys.exit(1)
        
    su_service = SuResource()

    if resourceId not in ('all') and not search:
        resource = su_service.RetrieveResourceCalendar(resourceId)
        for k,v in resource.iteritems():
            print "%-30s: %s" % (k, v)
        sys.exit(0)
    
    # Find all resource or find a match
    all_resources = su_service.RetrieveAllResourceCalendars()
    all_resources.sort(key=operator.itemgetter('resourceId'))
    if search:
        for r in all_resources:
            for k,v in r.iteritems():
                if k == 'resourceId' and v.find(resourceId) >= 0:
                    print "%-30s: %s\n" % (k, v)
    else:
        for r in all_resources:
            for k,v in r.iteritems():
                print "%-30s: %s\n" % (k, v)
        

if __name__ == '__main__':
    main(*sys.argv[1:])
