#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""
    create_group: create a Google Apps group

Usage: create_group [options] owner1 owner2 ...

Options:
        -g / --groupid=[groupid]
                Required. groupid to create.
        -n / --groupname=[groupname]
                Required. A short name for the group to be created.
        -d / --description=[short description of the group ]
                Required. A short descript of the group.
        -p / --emailpermission=[owner|member|domain|anyone]
                Optional. Default is to allow owner to post.
        -q / --quiet
                quiet mode. No output. Default is False.
        -h / --help
                Print this message and exit
    
Create a Google Apps group in primary domain. Groupid has to be longer than
8 characters and in stanford.edu domain. Google does not require owners to
have an account in the domain, but it is better that you use the primary 
account name as the owner to avoid potential problems. 

Example: create_group -g testgroup -n "Test group" -d "This is for test only" sunetid1 sunetid2

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import time

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email, friendly_domain_exception_exit
from lib.SuService import SuGroups
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Create a Google apps group."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqd:g:n:p:', ['help', 'quiet','groupid=', 'groupname=','description=','emailpermission='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupid = groupname = description = ''
    email_permission = 'Owner'
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            groupid,domain = parse_email(arg)
        elif option in ('-n', '--groupname'):
            groupname = arg
        elif option in ('-d', '--description'):
            description = arg
        elif option in ('-p', '--emailpermission'):
            email_permission = arg.capitalize()
        elif option in ('-q', '--quiet'):
            quiet = True

    if not (groupid and groupname and description):
        msg = "Missing groupid, groupname, or description.\n"
        sys_exit(1, msg)
        
    if domain and domain.lower() != gdata_cfg.DOMAIN:
        msg = "Groupid has to be in "+  gdata_cfg.DOMAIN + " domain."
        sys_exit(1,msg)

    if len(args) < 1:
        msg = "At least one owner is required."
        sys_exit(1,msg)
    else:
        owners = [x.lower() for x in args]

    # Make sure group names are not starting with reserved key words
    for k in gdata_cfg.RESERVED_GRROUP_PREFIX:
        if groupid.find(k,0) >= 0:
            msg = k + " is a reserved key word."
            sys_exit(1,msg)
    
    if len(groupid) < gdata_cfg.GROUP_NAME_MINIMUM_LEN:
        msg = "groupid has to be longer than %d." % gdata_cfg.GROUP_NAME_MINIMUM_LEN
        sys_exit(1,msg)

    if email_permission not in ('Owner', 'Member', 'Domain', 'Anyone'):    
        msg = "Email Permission should be one of the: 'owner', 'member', 'domain', 'anyone'" 
        sys_exit(1,msg)

    su_service = SuGroups()

    group_entry = su_service.CreateGroup(groupid,groupname,description,email_permission)
    for owner in owners:
        try:
            su_service.AddOwnerToGroup(owner, groupid)
        except AppsForYourDomainException, e:
            msg = friendly_domain_exception_exit(e,False)
            print "Skipping owner %s: %s" % (owner, msg)

        time.sleep(1)

    if not quiet:
        print "\n%s is created:" % (groupid)
        group_entry = su_service.RetrieveGroup(groupid)
        for k,v in group_entry.iteritems():
            print "%-30s: %s" % (k, v)

if __name__ == '__main__':
    main(*sys.argv[1:])
