#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University


""" Get all user account name in the domain.

Usage: get_all_users [options]
    -s / --suspended
            Get all suspended accounts.
    -a / --admin     
            Get all accounts with administration privileges.
    -c / --classofservice=[cos1|cos2|cos3] 
            Get all active users in the given class of service.
    -A / --ALL       
            Get all active users in the domain.
    -h / --help
            Print this help message.

"""

__author__ = 'sfeng@stanford.edu'

import getopt
import sys

import paths
import gdata_cfg
from lib.Utils import sys_exit, str2bool, parse_email
from lib.SuService import SuUsers, SuOrganization

def short_desc():
    return "Get all, suspended, or administration accounts."

def get_all_cos_users(cos,suspended,admin):
    org_service = SuOrganization()
    customerid = org_service.get_customer_id()
    user_service = SuUsers()
      
    user_list = org_service.RetrieveOrgUnitUsers(customerid, cos)
    for user in user_list:
        u, d = parse_email(user['orgUserEmail']) # Remove domain part
        user_entry = SuUsers().RetrieveUser(u)
        if suspended:
            if str2bool(user_entry.login.suspended):
                print user['orgUserEmail']
        elif admin:
            if str2bool(user_entry.login.admin):
                print user['orgUserEmail']
        else:
            if not str2bool(user_entry.login.suspended):
                print user['orgUserEmail']
        sys.stdout.flush()

def get_all_users(suspended,admin):
    su_service = SuUsers()
    for feed in su_service.GetGeneratorForAllUsers():
        for user in feed.entry:
            if suspended:
                if str2bool(user.login.suspended):
                    print user.login.user_name
            elif admin:
                if str2bool(user.login.admin):
                    print user.login.user_name
            else:
                if not str2bool(user_entry.login.suspended):
                    print user.login.user_name 
            sys.stdout.flush()
                
def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hsaAc:', ['help','suspended','admin', 'ALL','classofservice='])
    except getopt.error, msg:
        sys.exit(0,msg)
        
    only_suspended = only_admin = all = False
    cos = None
    
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-s', '--suspended'):
            only_suspended = True
        elif option in ('-a', '--admin'):
            only_admin = True
        elif option in ('-A', '--ALL'):
            all = True
        elif option in ('-c', '--classofservice'):
            cos = arg

    if not (all or cos):
        msg = "Either -A or -c <cos> is required."
        sys_exit(1, msg)

    print "This may take awhile..."
    sys.stdout.flush()
    if cos:
        try:
            cos = gdata_cfg.CLASSOFSERVICES[cos]
        except KeyError:
            msg = "Wrong class of service.\n"
            sys_exit(1, msg )
        get_all_cos_users(cos,only_suspended,only_admin)
    elif all:
        get_all_users(only_suspended,only_admin)
         
if __name__ == '__main__':
    main(*sys.argv[1:])
