#!/usr/bin/python
#
# Copyright 2012
#         The Board of Trustees of the Leland Stanford Junior University

"""audit-create-monitor: Create an email monitor, which forwards the source user's email to an authorized user.

Usage: audit_delete_monitor [options]

Options:
        -s / --source_user <souce username>
                 Create a monitor for the user whose email will be audited
        -d / --destination_user <destination username> 
        -h / --help
                Print this message and exit

        Example: audit-delete-monitor -s sourceid -d destid
                Wlll delete a monitor that forwards sourcid's email to destid's mailbox.
        
        The source and destination accounts have to be in the same domain.
"""
__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit 
from lib.SuService import SuAudit

def short_desc():
    return "Delete a user account monitor."

def main(*args):
    # Parse command line options
    try:
        opts,args = getopt.getopt(args, 's:d:h', ['help', 
             'source_user=','destination_user='])
    except getopt.error, msg:
        sys_exit(1,msg)

    source_user = None
    destination_user = None

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-s', '--source_user'):
            source_user = arg
        elif option in ('-d', '--destination_user'):
            destination_user = arg

    if not (source_user and destination_user):
        msg = "source and destination users are required."
        sys_exit(1,msg)

    # Create a authenticated Google service object
    su_service = SuAudit()
    
    """
    def createEmailMonitor(self, source_user, destination_user, end_date, 
        begin_date=None, incoming_headers_only=False, 
        outgoing_headers_only=False, drafts=False, 
        drafts_headers_only=False, chats=False, 
        chats_headers_only=False):
    """
    result = su_service.getEmailMonitors(source_user)
    found_monitor = False
    print "\nExisting monitors for %s are:\n" % source_user
    for r in result:
        for k,v in r.iteritems():
            #print k, ":", v
            print k,v
            if k == 'destUserName' and v == destination_user:
                found_monitor = True
        print "\n"
    
    if found_monitor:
        su_service.deleteEmailMonitor(source_user,destination_user)
        print "Removed forwarding from %s to %s." % (source_user,destination_user)
    else:
        print "No monitor is found from %s to %s." % (source_user,destination_user)

if __name__ == '__main__':
        main(*sys.argv[1:])
