#!/usr/bin/python
#
# Copyright 2012
#         The Board of Trustees of the Leland Stanford Junior University

"""
    update_resource: update a Google Apps calendar resource

Usage: update-resource --resource "resourceid" --name "resourceCommonName" --description "Descriptions"

Options:
        -r / --resource=[resourceid]
                Required. resourceid to update.
                A resource name shall begin with the following prefix:
                cal_lo_building_room_ ... for location
                cal_eq_<name>_ .... for equipment
                cal_sc_... for shared resources, such as shared calendars
        -c / --capacity=[number]
               Optional. Capacity of the room.
        -n / --name=[resource display name]
                Optional. New resource name.
        -z / --zimbra=[resource display name] 
               Not implemented yet.
        -d / --description=[Description and information about the resource ]
                Optional. New description.
        -q / --quiet
                print updated resource information, default is to print.
        -h / --help
                Print this message and exit
    
Upaate a Google Apps calendar resource name or description. resoruceId cannot be modified.

Example: 
    update_resource --resource cal_lo_polya_hall_152 --description "Polya Hall 152; Capacity 16; Contact sfeng@stanford.edu"

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit
from lib.SuService import SuResource
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Update a Google Apps calendar resource."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqr:c:n:d:', ['help','resourceid=', 'capacity=','name=','description='])
    except getopt.error, msg:
        sys_exit(1,msg)

    resourceId = resourceCommonName = description = capacity =''
    quiet = False
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-r', '--resourceid'):
            resourceId = arg.lower()
        elif option in ('-n', '--name'):
            resourceCommonName = arg
        elif option in ('-d', '--description'):
            description = arg
        elif option in ('-c', '--capacity'):
            capacity = arg
        elif option in ('-q', '--quiet'):
            quiet = True

    if not resourceId:
        msg = "Resource id is required."
        sys_exit(1,msg)
    else:
        prefix = resourceId[:7] 
        try:
            resourceType = gdata_cfg.RESOURCE_MAP[prefix]
        except KeyError:
            print "Wrong resource type. Should be one of the following:"
            print ', '.join(gdata_cfg.RESOURCE_MAP.keys())
            sys.exit(1)
        resourceType = gdata_cfg.RESOURCE_MAP[prefix]
        
    if not resourceCommonName and not description and not capacity:
        print "Nothing to change."
        sys.exit(1)
    
    if capacity:
        try: 
            capacity = int(capacity)
        except ValueError:
            print "Capacity should be a number."
            sys.exit(1)
        else:
            if capacity < 1:
                print "Capacity should be larger than 1"
                sys.exit(1)
                
    su_service = SuResource()
    resource = su_service.RetrieveResourceCalendar(resourceId)
    if resourceCommonName:
        newResourceCommonName = resourceCommonName.title()
        if capacity: 
            newResourceCommonName = "%s (capacity:%s)" %(newResourceCommonName, capacity)             
    else:
        newResourceCommonName = resource['resourceCommonName']
        at_sign = newResourceCommonName.find('(')
        if at_sign < 1:
            # No capacity before
            if capacity:
                newResourceCommonName = newResourceCommonName.replace(' (','(')
                newResourceCommonName = "%s (capacity:%s)" %(newResourceCommonName, capacity)
        elif capacity:
            newResourceCommonName = "%s (capacity:%s)" %(newResourceCommonName[:at_sign], capacity)
        
    if not description:
        description = resource['resourceDescription']
    else:
        if description.find('ph:') >= 0:
            description.replace(': ',':')
            telephone = description[4:].split(';')[0]
        #resourceCommonName = data['displayName']


    su_service.UpdateResourceCalendar(resourceId, newResourceCommonName,description,resourceType)

    if not quiet:
        print "\n%s is updated:" % (resourceId)
        resource = su_service.RetrieveResourceCalendar(resourceId)
        for k,v in resource.iteritems():  
            print "%-30s: %s" % (k, v)

if __name__ == '__main__':
    main(*sys.argv[1:])
