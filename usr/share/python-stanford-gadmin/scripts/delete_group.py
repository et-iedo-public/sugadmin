#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""
    delete_group: delete a Google Apps group

Usage: create_group [options]

Options:
        -g / --groupid=[groupid]
        -q / --quiet 
                Quiet mode. No output
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

def short_desc():
    return "Delete a group."

import sys
import getopt

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGroups
import gdata_cfg

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqg:p:', ['help', 'quiet','groupid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    group_id = ''
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            group_id = arg
        elif option in ('-q', '--quiet'):
            quiet = True

    if not group_id:
        msg = 'group_id is required.'
        sys_exit(1,__doc__)

    su_service = SuGroups()
    su_service.DeleteGroup(group_id)
    if not quiet:
            print group_id + ' is deleted.'

if __name__ == '__main__':
    main(*sys.argv[1:])
