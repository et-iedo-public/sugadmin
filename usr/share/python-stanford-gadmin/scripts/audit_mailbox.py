#!/usr/bin/python

# Copyright 2011 by the Board of Trustees of the Leland Stanford Junior University

"""
    audit-mailbox: Create a request to download mailbox and manage the request

Usage: audit_mailbox [options] [ <userid ]

Options:
        -c / --create
                Create a request to export mailbox for the given userid
        -g / --get <requestid|ALL> 
                 Get the status of a request or all requests
        -d / --delete <requestid|ALL>
                 Delete a request or all requests.
        -s / --start yyyy-MM-dd HH:mm 
                 Date of earliest emails to export, defaults to date of account's 
                 creation date.
        -e / --end yyyy-MM-dd HH:mm
                 Date of latest emails to export, defaults to current date.
        -i / --include_deleted: boolean, default false.
                 Whether to include deleted emails in export, mutually exclusive 
                 with search_query.
        -q / --query: string
                 Matched emails will be exported, mutually exclusive with 
                 include_deleted.
        -o / --only_headers_only
                 Export message headers only. Default is false.
        -h / --help
                Print this message and exit.

        username
                The user whose mailbox export is being requested. Ignored if the requestid is ALL.

        Example: audit-mailbox -c sfeng
                Will create a request to export sfeng's mailbox from the account creation time to the 
                current time.
"""

__author__ = 'sfeng@stanford.edu'

def short_desc():
        return "Create a request to export mailbox and manage the request"

import sys
import getopt

import paths
from lib.Utils import sys_exit,remove_domain
from lib.SuService import SuAudit

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'g:s:e:q:d:coih', ['help', 'create', 'delete=', 'get=',
                                 'start=', 'end=', 'query='])
    except getopt.error, msg:
        sys_exit(1, msg)

    action = None
    start = None
    end = None
    query = None
    headers_only = False
    include_deleted = False
    requestid = None

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-c', '--create'):
            action = 'create'
        elif option in ('-g', '--get'):
            action = 'get'
            requestid = arg
        elif option in ('-d', '--delete'):
            action = 'delete'
            requestid = arg
        elif option in ('-s', '--start'):
            start = arg 
        elif option in ('-e', '--end'):
            end = arg 
        elif option in ('-q', '--query'):
            query = arg 
        elif option in ('-i', '--include_deleted'):
            include_deleted = True
        elif option in ('-o', '--only_headers'):
            headers_only = True

    if not action:
        msg = "Nothing to do. Missing command."
        sys_exit(1, msg)

    if len(args) <> 1 and requestid != 'ALL':
        msg = "Userid is required."
        sys_exit(1, msg)
    elif requestid != 'ALL':
        user = remove_domain(args[0])

    su_service = SuAudit()

    if action == 'create':
        result = su_service.createMailboxExportRequest(user, start, end, include_deleted, query, headers_only)
        for k, v in result.iteritems():
            print k, ": ", v 
    elif action == 'get':
        if requestid == 'ALL':
            result = su_service.getAllMailboxExportRequestsStatus()
            for entry in result:
                print "----"
                for k, v in entry.iteritems():
                    print k, ": ", v 
                print "----"
        else:
            result = su_service.getMailboxExportRequestStatus(user, requestid)
            for k, v in result.iteritems():
                print k, ": ", v 
    else:
        result = su_service.deleteMailboxExportRequest(user, requestid)

if __name__ == '__main__':
        main(*sys.argv[1:])
