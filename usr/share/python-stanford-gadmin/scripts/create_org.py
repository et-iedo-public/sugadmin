#!/usr/bin/python

"""
create_org: Create a organization unit.

Usage: create_org [options]

Options:
        -n / --name=[orgname]
        -d / --description=[description]
        -h / --help
                Print this message and exit

This script creates an organization unit. For the current GoogleApps 
implementation, we use organization as a grouping method to control
what services will be enabled for the users in this organization. The
organization can be a real organization, but it doesn't have to be.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit
#from lib.Directory import directory_query
from lib.SuService import SuOrganization

def short_desc():
    return "Create an organization unit."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hn:d:', ['help', 'name=', 'description='])
    except getopt.error, msg:
        sys_exit(1, msg)

    name = description = '' 

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-n', '--name'):
            name = arg # We don't do any checking for now.
        elif option in ('-d','--description'):
            description = arg

    if not (name and description ):
        msg = "name and description are required."
        sys_exit(1,msg)
 
    # Parent org is always the top domain
    parent_org_unit_path='/'

    su_service = SuOrganization()
    customerid = su_service.RetrieveCustomerId()
    customerid = customerid['customerId']
    su_service.CreateOrgUnit(customerid, name, parent_org_unit_path, description) 
    print "%s is created." % ( name )

if __name__ == '__main__':
    main(*sys.argv[1:])
