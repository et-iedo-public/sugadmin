#!/usr/bin/env python
#
# Copyright 2012
#     The Board of Trustees of the Leland Stanford Junior University

"""Manage OAuth2 token and store it in pickle database file for reuse

Usage: oauth2_admin [options]

Options:
        -r / --request oauth2 token
        -p / --print oauth2 token stored in database
        -d / --destroy oauth2 token stored in database
        -h / --help
           Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import sys
import pickle
import gdata.apps.service

import paths
import gdata_cfg
import getopt
from lib.Utils import sys_exit

def short_desc():
    return "Manage OAuth2 token."

def doRequestOAuth2():
    # No plan to use Group Settings commands, so no client id is needed
    #consumer_secret = gdata_cfg.CONSUMER_SECRET
    #consumer_key = domain = gdata_cfg.DOMAIN
    domain = gdata_cfg.DOMAIN
    consumer_key = gdata_cfg.CONSUMER_KEY
    consumer_secret = gdata_cfg.CONSUMER_SECRET
    fetch_params = gdata_cfg.OAUTH_TOKEN_NAME
    scopes = gdata_cfg.SCOPES
    apps = gdata.apps.service.AppsService(domain=domain)
    apps.source = gdata_cfg.OAUTH_TOKEN_NAME
    apps.SetOAuthInputParameters(gdata.auth.OAuthSignatureMethod.HMAC_SHA1,
        consumer_key=consumer_key, consumer_secret=consumer_secret)
    try:
        request_token = apps.FetchOAuthRequestToken(scopes=scopes,
                      extra_parameters=fetch_params)
    except gdata.service.FetchingOAuthRequestTokenFailed, e:
        print "Error: %s" % e
        sys.exit(6)
    url_params = {'hd': domain}
    url = apps.GenerateOAuthAuthorizationURL(request_token=request_token,
          extra_params=url_params)

    print 'Please visit this URL to authorize the application:'
    print url
    # Get the verification code from the standard input.
    raw_input('Press return once you granted the access:').strip()
    try:
        #apps.token.get_access_token(code)
        final_token = apps.UpgradeToOAuthAccessToken(request_token)
    except gdata.service.TokenUpgradeFailed:
        msg = 'Failed to upgrade the token.'
        sys_exit(1,msg)
    oauth_filename = gdata_cfg.OAUTH_TOKEN_FILE
    f = open(oauth_filename, 'wb')
    f.write('%s\n' % (domain,))
    pickle.dump(final_token, f)
    f.close()
    print "Stored %s in %s." % (final_token.get_token_string(), oauth_filename)
    
def doShowOAuth2():
    file=gdata_cfg.OAUTH_TOKEN_FILE
    file = open(file, 'rb')
    domain = file.readline()[0:-1]

    token = pickle.load(file)
    file.close()
    secret = token.oauth_input_params._consumer.secret
    print 'Google Apps Domain: %s' % domain
    print "Client ID: %s\nSecret: %s" % (token.oauth_input_params._consumer.key,
        secret[-4:].rjust(len(secret), "*"))
    print 'Scopes:'
    for scope in token.scopes:
        print '  %s' % scope
    
def doDestroyOauth2():
    print "For security reason, please go to Google Apps console to revoke the oauth token."


def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hprds', ['help', '--print', '--request', '--destroy'
          '--show'])
    except getopt.error, msg:
        sys_exit(1, msg)

    request = destroy = show = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-r', '--request'):
            request = True
        elif option in ('-d', '--destroy'):
            destroy = True
        elif option in ('-p', '--print'):
            show = True

    if request:
        doRequestOAuth2()
    elif show:
        doShowOAuth2()
    elif destroy:
        doDestroyOauth2()
    else:
        doShowOAuth2()
        
if __name__ == '__main__':
    main(*sys.argv[1:])

