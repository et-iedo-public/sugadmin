#! /usr/bin/python

"""Reset a user's password.

Usage: reset_password [options]

Options:
        -u / --username=[username]
        -p / --password=[password]
        -i / --ipaddress=[ipaddress]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import hashlib

import paths
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuUsers

def short_desc():
    return "Reset a user's password."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args,'hu:p:i:', ['help', 'username=', 'password=','ipaddress='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = password = ''
    # From which address the user's request is coming from. 
    # Currently not used by this program - will need to use database 
    # to log this, among other information. 
    ipaddress = '0.0.0.0'

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-u', '--username'):
            username,domain = parse_email(arg)
        elif option in ( '-p', '--password'):
            password = arg
        elif option in ( '-i', '--ipaddress'):
            ipaddress = arg

    if not username:
        msg = 'Please enter username'
        sys_exit(1,msg)
    
    if not ( password ):
        msg = 'Nothing to update'
        sys_exit(1,msg)

    su_service = SuUsers()
    user_entry = su_service.RetrieveUser(username)
    
    if password:
            user_entry.login.password = hashlib.sha1(password).hexdigest()
            user_entry.login.hash_function_name = 'SHA-1'
    
    # user_entry.login.admin = 'true'
    # user_entry.login.agreed_to_terms = 'false'

    su_service.UpdateUser(username,user_entry)
    print '%s is updated' % ( user_entry.login )

if __name__ == '__main__':
    main(*sys.argv[1:])
