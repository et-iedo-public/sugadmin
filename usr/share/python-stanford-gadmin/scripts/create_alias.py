#!/usr/bin/python

"""
create_alias: Create a user alias in subdomain.

Usage: create_alias [options]

Options:
        -u / --username=[username]
        -a / --alias=[aliasemail]
        -h / --help
                Print this message and exit

An alias can be associated with a different domain used by the user's 
primary email address. This script creates an alias in sub-domain. 
You need to use create_sendas to create the the relations between 
an alias and a primary account. 

"""
__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuAlias

def short_desc():
    return "Create a subdomain alias, associate it with a primary account."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:a:', ['help', 'username=', 'aliasemail='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = domain = aliasemail = subdomain = alias = '' 

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username,domain = parse_email(arg)
        elif option in ('-a','--aliasemail'):
            aliasemail = arg
            alias,subdomain = parse_email(arg)

    if  not ( username and alias ):
        msg = "username and subdomain alias are required."
        sys_exit(1,msg)
 
    if not domain:
        domain = gdata_cfg.DOMAIN
    elif domain != gdata_cfg.DOMAIN:
        msg = "username needs to be in primary domain."
        sys_exit(1,msg)
    
    useremail = username + '@' + domain
    
    if not subdomain:
        msg = "Subdomain is required."
        sys_exit(1,msg)

    if subdomain.lower().find(gdata_cfg.SUBDOMAIN) < 0:
        msg = "alias address needs to be %s." % (gdata_cfg.SUBDOMAIN)
        sys_exit(1,msg)
  
    su_service = SuAlias()
    # CreateAlias require fully qualified email address for both primary account and aliasname
    su_service.CreateAlias(useremail,aliasemail,subdomain)
    print "%s is created for %s." % ( aliasemail, username)

if __name__ == '__main__':
    main(*sys.argv[1:])
