#!/usr/bin/env python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""Move a user from one service class to another

This script moves an existing user in Google Apps domain from one service class
to another. If a file containing a list of sunetids is given at the command
line, move all users in the list to another service class.

Options:
        -u / --username=[username]
            Primary account name to be created.
        -f / --file=[file]
            File with list of user's sunetids to be moved.
        -c / --classofservice=[class of service]
            Class of service. 
            cos1: Full services enabled (primarily for GSB) 
            cos2: No email and calendar (staff/faculty) 
            cos3: Full services enabled (undergraduate students)  
            cos4: Reserved
            cos5: Core services on; consumer services off (for users who 
                  opt-out of consumer services)
            ClassOfServiceInactive: Inactive accounts, no services
            ClassOfTransfer: ShuttleCloud market app enabled for migration.
        -h / --help
            Print this message and exit.

"""
import sys
import getopt
import time

import paths
import gdata_cfg
from lib.Utils import sys_exit
from lib.Utils import parse_email,readlines,add_domain
from lib.SuService import SuUsers, SuOrganization
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Move a user from one service class to another."

def main(*args):
    """ Move users between class of services    """
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:f:c:', ['help', 'username=', 'file=','classofservice='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = file = ''
    cos = ''

    # Process options
    for option, arg in opts:
        if option in ('-u', '--username'):
            username,domain=parse_email(arg)
        elif option in ('-f', '--file'):
            file = arg
        elif option in ('-c', '--classofservice'):
            cos = arg
        elif option in ('-h', '--help'):
            sys_exit(0,__doc__)

    if username and file:
        msg = "Use username or file, not both."
        sys_exit(1,msg)

    if not username and not file:
        msg = "username or file is required."
        sys_exit(1,msg)
    
    if not cos:
        msg = "class of service is required."
        sys_exit(1,msg)

    try:
        cos = gdata_cfg.CLASSOFSERVICES[cos]
    except KeyError:
        msg = "Wrong class of service.\n"
        sys_exit(1, msg )

    accounts = []
    rc = 0
    if username:
        accounts.append(username)
    else:
        lines = readlines(file)
        for l in lines:
            accounts.append(l.rstrip())

    for user in accounts:
        if not SuUsers().is_ga_user(user):
            print "%s does not exists" % (user)
            rc = 1
        else:
            try:
                user = add_domain(user)
                su_service = SuOrganization()
                su_service.move_to_cos(user,cos)
                print "%s is moved to %s." % (user, cos)
            except AppsForYourDomainException, e:
                print e
                rc = 1

    sys.exit(rc)

# Only execute the code when it's called as a script, not just imported.
if __name__ == '__main__':
    main(*sys.argv[1:])

