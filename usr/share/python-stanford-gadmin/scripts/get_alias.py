#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""\nGiven a subdomain address, show its primary email address.

Usage: get_alias [options]

Options:
        -a / --email=[aliasemail]
                subdomain email address to query. 
        -d / --domain=[domain]
        -q / --quiet 
           Quiet mode. Use it to check if an alias exists.
           Return 0 if alias exit, 1 if not.
        -h / --help
"""
__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

from  gdata.apps.service import AppsForYourDomainException
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuAlias
import gdata_cfg

def short_desc():
    return "Given a subdomain alias, retrieve its user's primary email."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqa:d:', ['help', 'quiet','email=', 'domain='])
    except getopt.error, msg:
        sys_exit(1,msg)

    quiet = False
    email = ''
    domain = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-a', '--email'):
            email = arg
        elif option in ('-d', '--domain'):
            domain = arg
        elif option in ('-q', '--quiet'):
            quiet = True
            
    if not email:          
        msg = 'Alias address is requird.'
        sys_exit(1,msg)
    
    if not domain:
        domain = gdata_cfg.DOMAIN 
    
    su_service = SuAlias()  
    result = su_service.RetrieveAlias(email, domain)
    print "user email: %s \nalias email: %s" % (result['userEmail'], result['aliasEmail'])

    # Comment out debugging code
    if False:
        try:
            result = su_service.RetrieveAlias(email, domain)
        except AppsForYourDomainException, e:
            print "status: %s \nreason: %s \ngoogle reply: %s\n" % (e[0]['status'], e[0]['reason'], e[0]['body'])
        else:    
            print "user email: %s \nalias email: %s" % (result['userEmail'], result['aliasEmail'])

if __name__ == '__main__':
    main(*sys.argv[1:])
