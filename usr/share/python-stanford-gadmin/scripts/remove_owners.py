#!/usr/bin/python

# Copyright 2011 by the Board of Trustees of the Leland Stanford Junior University

"""Remove owners of a group.

Remove owners from a group.

remove_owners [options] owner1 owner2...

Options:
        -g / --groupid=[groupid|groupid@fqdn]
                Remove owners from the given group. The default is the group in 
                primary domain unless fully qualified subdomain is given.
        -h / --help
                Print this message and exit

        owner1 owner2 .. owners to be removed from the group.
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator
import time

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGroups
import gdata_cfg

def short_desc():
    return "Remove owner(s) from a group."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hg:', ['help', 'groupid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupid = ''
    owners = []
    
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            groupid = arg
            
    if not groupid:
        msg = 'groupid is required.'
        sys_exit(1,msg)
    if args:
        owners = args
    else:
        msg = "At least one owner is required."
        sys_exit(1, msg)

    if not groupid:
        msg = 'groupid is required.'
        sys_exit(1,msg)

    service = SuGroups()
    if service.RetrieveGroup(groupid):
        for owner in owners:
            if service.IsOwner(owner,groupid): 
                service.RemoveOwnerFromGroup(owner, groupid)
                print "Removed owner %s from %s." % (owner, groupid)
            else:
                print "%s is not the owner of %s." % (owner, groupid)
            time.sleep(1)
    else:
        print "%s doesn't exist." % (groupid)

if __name__ == '__main__':
    main(*sys.argv[1:])
