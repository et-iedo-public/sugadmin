#!/usr/bin/python

"""Check if a member exists in the group.

Check if a member exists in the group. Return true if the member exists in 
the group. False otherwise

Usage: is_member [options] member ...

Options:
        -g / --groupid=[groupid]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGoogleService

def short_desc():
    return "Check if a member exists in the group."
    
def main():

    # Parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hm:g:', ['help', 'groupid=', 'members='])
    except getopt.error, msg:
        sys_exit(1,msg)

    group_id = ''
    members = []
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            group_id = arg
        elif option == '-q':
            quiet = True

    if not group_id:
        msg = "Group id is required"
        sys_exit(1, msg)

    if args:
        members = args
    else:
        msg = "Member is required."
        sys_exit(1, msg)

    # Create a authenticated Google service object 
    su_service = SuGoogleService()
    if not su_service.RetrieveGroup(group_id):
        msg = group_id + " does not exist"
        sys_exit(1, msg)

    for member in members:
        if su_service.IsMember(member, group_id):
            print member + ' is a member of ' + group_id
        else:
            print member + ' is not a member of ' + group_id

if __name__ == '__main__':
    main()
