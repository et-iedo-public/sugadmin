#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Get Administrative domain settings. 

Usage: adminsettings [options]

Options:
    -p / --pin 
        Get customer PIN number
    -h / --help
        Print this message.
       
    When --pin is given, get customer PIN number used for support. Othewise, 
    get all domain  major settings.
    
"""

__author__ = 'sfeng@stanford.edu'

import getopt
import sys
import time

import paths
import gdata_cfg
from gdata.service import DEFAULT_NUM_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF, RanOutOfTries
from lib.SuService import SuAdmin
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Get domain settings."

def get_general_settings(su_service):
    """ General settings """

    print "Getting domain settings...."
    sys.stdout.flush()
    general_settings = {
        'Google Apps Domain':    su_service.domain,
        'Maximum Users':         su_service.GetMaximumNumberOfUsers(),
        'Current Users':         su_service.GetCurrentNumberOfUsers(),
        'Domain is Verified':    su_service.IsDomainVerified(),
        'Customer PIN':          su_service.GetCustomerPIN(),
        'Domain Creation Time':  su_service.GetCreationTime(),
    }

    delay = 0
    for k,v in general_settings.iteritems():
        print "%-30s: %s" % (k, v)
        sys.stdout.flush()

def get_sso_settings(su_service):
    """ SSO settings """

    ssosettings = su_service.GetSSOSettings()
    ssomapkeyval = {'SSO Enabled': ssosettings['enableSSO'],
      'SSO Signon Page': ssosettings['samlSignonUri'],      'SSO Logout Page': ssosettings['samlLogoutUri'],
      'SSO Password Page': ssosettings['changePasswordUri'],
      'SSO Whitelist IPs': ssosettings['ssoWhitelist'],
      'SSO Use Domain Specific Issuer': ssosettings['useDomainSpecificIssuer'],
    }
    for k,v in ssomapkeyval.iteritems():
        print "%-30s: %s" % (k, v)
    time.sleep(1)

def get_mx_settings(su_service):
    """ MX verification status """
    mxverificationstatus = su_service.GetMXVerificationStatus()
    print "%-30s: %s" % ('MX Verification Verified', mxverificationstatus['verified'])
    print "%-30s: %s" % ('MX Verification Method', mxverificationstatus['verificationMethod'])
    time.sleep(1)

def get_cname_settings(su_service):
    """ CNAME verification status """

    cnameverificationstatus = su_service.GetCNAMEVerificationStatus()
    print '%-30s: %s' % ('CNAME Verification Record Name', cnameverificationstatus['recordName'])

    print '%-30s: %s' % ('CNAME Verification Verified', cnameverificationstatus['verified'])
    print '%-30s: %s' % ('CNAME Verification Method', cnameverificationstatus['verificationMethod'])
    time.sleep(1)

def get_outbound_gateway(su_service):
    """ Outbound gateway """

    outboundgatewaysettings = su_service.GetOutboundGatewaySettings()
    try:
        print "%-30s: %s" % ('Outbound Gateway Smart Host', outboundgatewaysettings['smartHost'])
    except KeyError:   
        pass
    try:
        print "%-30s: %s" % ('Outbound Gateway Mode', outboundgatewaysettings['smtpMode'])
    except KeyError:
        pass

def main(*args):
    """ Main """
    try:
        opts, args = getopt.getopt(args, 'hp', ['help', 'pin'])
    except getopt.error, msg:
        print msg
        sys.exit(1)

    pin = False
    for option, arg in opts:
        if option in ('-h', '--help'):
            print __doc__
            sys.exit(0)
        elif option in ('-p', '--pin'):
            pin = True
    
    su_service = SuAdmin()
    if pin:
        p = su_service.GetCustomerPIN(),
        print "Customer PIN: %s" % (p)
    else:
        get_general_settings(su_service)
        # Google seemed take away this from admin settings API.
        # get_cname_settings(su_service), Google seemed take away this from admin settings API.
        get_mx_settings(su_service)
        get_sso_settings(su_service)
    
if __name__ == '__main__':
    main(*sys.argv[1:])
