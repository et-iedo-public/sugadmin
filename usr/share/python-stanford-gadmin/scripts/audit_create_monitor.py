#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""audit-create-monitor: Create an email monitor, which forwards the source user's email to an authorized user.

Usage: audit_create_monitor [options]

Options:
        -s / --source_user <souce username>
                 Create a monitor for the user whose email will be audited
        -d / --destination_user <destination username> 
                 The user to receive the audited email 
        -b / --begin <yyyy-MM-dd HH:mm>
                 The date the audit will begin. Default is current time
        -e / --end <yyyy-MM-dd HH:mm>
                 The date the audit will stop. Required.
        -i / --incoming_headers_only
                 Audit the incoming headers only. Default is false.
        -o / --outgoing_headers_only
                 Audit the outgoing headers only. Default is false.
        -h / --help
                Print this message and exit

        Example: audit-create-monitor -s sourceid -d destid
                Wlll create a request to forward sourcid's email to destid's mailbox.
        
        The source and destination accounts have to be in the same domain.
"""
__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit 
from lib.SuService import SuAudit

def short_desc():
    return "Create request to monitor a user's mailbox."

def main(*args):
    # Parse command line options
    try:
        opts,args = getopt.getopt(args, 's:d:b:e:ioh', ['help', 
             'source_user=','destination_user=',
            'begin=','end=','incoming_headers_only',
            'outgoing_headers_only'])
    except getopt.error, msg:
        sys_exit(1,msg)

    source_user = None
    destination_user = None
    incoming_headers_only = False
    outgoing_headers_only = False
    begin = None
    end = None

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-s', '--source_user'):
            source_user = arg
        elif option in ('-d', '--destination_user'):
            destination_user = arg
        elif option in ('-b', '--begin'):
            begin = arg
        elif option in ('-e', '--end'):
            end = arg
        elif option in ('-i', '--incoming_headers_only'):
            incoming_headers_only = True
        elif option in ('-o', '--outgoing_headers_only)'):
            outgoing_headers_only = True

    if not (source_user and destination_user):
        msg = "source and destination users are required."
        sys_exit(1,msg)

    if not end:
        msg = "end date is required."
        sys_exit(1,msg)

    # Create a authenticated Google service object
    su_service = SuAudit()
    
    """
    def createEmailMonitor(self, source_user, destination_user, end_date, 
        begin_date=None, incoming_headers_only=False, 
        outgoing_headers_only=False, drafts=False, 
        drafts_headers_only=False, chats=False, 
        chats_headers_only=False):
    """

    result = su_service.createEmailMonitor(source_user,destination_user,
                                     end,begin,incoming_headers_only,outgoing_headers_only)
    print "Created monitor on account %s." % source_user
    for k,v in result.iteritems():
            print k, ": ", v 
    print "\n"

if __name__ == '__main__':
        main(*sys.argv[1:])
