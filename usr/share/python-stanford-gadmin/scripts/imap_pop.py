#!/usr/bin/python

"""
imap_pop: enable / disable imap or pop service

Usage: imap_pop [options]

Options:
    -u / --username=[username]
    --service=[imap/pop]
    --enable=[true/false]
    -h / --help
        Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import getopt
import sys
import paths

from lib.Utils import sys_exit
from lib.Utils import str2bool
from lib import EmailSettings

def main():
  # Parse command line options
  try:
    opts, args = getopt.getopt(sys.argv[1:], 'hu:', ['help','username=','service=','enable='])
  except getopt.error, msg:
    sys_exit(1,msg)

  username = service = enable = msg = ''

  # Process options
  for option, arg in opts:
    if option in ('-h', '--help'):
      sys_exit(0,__doc__)
    elif option in ('-u', '--username'):
      username = arg
    elif option == '--service':
      service = arg
    elif option == '--enable':
      enable = arg

  # Error checking
  if not username:
    msg = __doc__ + 'Please give username'
  elif service not in ('pop', 'imap'):
    msg = __doc__ + 'Service name has to be either pop or imap'
  elif enable.lower() not in ('true', 'false'):
    msg = __doc__ + 'enable has to be either true or false'

  if msg: sys_exit(1, msg )

  enable = str2bool(enable)

  user_properties = EmailSettings.EmailSettings()
  if service == 'imap':
      user_properties = user_properties.UpdateImap(username,enable)
  else :
      user_properties = user_properties.UpdatePop(username,enable)

  for k in user_properties.keys():
      print "%s: %s = %s" %(service, k, user_properties[k])
  
if __name__ == '__main__':
  main()
