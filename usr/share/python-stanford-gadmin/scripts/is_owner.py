#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Check if a user is an owner of a group or not

Check if a user is an owner of a group or not. Return true (0) if the
user is a owner of the group, false (1) if not.

Usage: is_owner [options]

Options:
    -g / --groupid=[groupid]
    -o / --ownerid[ownerid]
    -h / --help
        Print this message and exit

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGroups

def short_desc():
    return "Check if a user is an owner of a group."
    
def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hvg:o:', ['help', 'verbose', 'groupid=', 'ownerid='])
    except getopt.error, msg:
        sys_exit(1, msg)

    groupid = ownerid = ''
    verbose = False
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-g', '--groupid'):
            groupid = arg
        elif option in ('-o', '--ownerid'):
            ownerid = arg
        elif option in ('-v', '--verbose'):
            verbose = True

    if not groupid:
        msg = 'groupid is required.'
        sys_exit(1,msg)
    if not ownerid:
        msg = 'ownerid is required.'
        sys_exit(1,msg)

    # Create a authenticated Google service object 
    su_service = SuGroups()

    #Call su service to get data
    su_service.RetrieveGroup(groupid)

    if su_service.IsOwner(ownerid, groupid):
        msg = ownerid + ' is a owner of ' + groupid
        status = 0
    else:
        msg = ownerid + ' is not a owner ' + groupid
        status = 1
    if verbose:
        sys_exit(status, msg)
    else: 
        sys_exit(status,'')

if __name__ == '__main__':
    main(*sys.argv[1:])
