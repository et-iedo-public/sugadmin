#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University


"""Get information about a group

Usage: get_group [options]

Options:
        -g / --groupaddr=[groupaddr]
             Display group information is the primary domain, or in subdomain if fqdn gievn.
        -q / --quiet
             No output. Can be used to check if a group exists.
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuGroups

def short_desc():
    return "Get information about a group."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqg:', ['help','quiet','groupaddr='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupid = ''
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupaddr'):
            groupaddr = arg
            groupid, domain = parse_email(arg)
        elif option == '-q':
            quiet = True

    if not groupid:
        msg = 'groupid is required.'
        sys_exit(1,msg)

    su_service = SuGroups()
    group_entry = su_service.RetrieveGroup(groupaddr)
    if not quiet:
        for k,v in group_entry.iteritems():
            print "%-20s: %s" % (k, v)

    sys.exit(0)

if __name__ == '__main__':
    main(*sys.argv[1:])
