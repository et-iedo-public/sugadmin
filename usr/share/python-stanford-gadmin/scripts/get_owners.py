#!/usr/bin/python

# Copyright 2011 by the Board of Trustees of the Leland Stanford Junior University

"""
    get_members: get a list of all members in the given group.

Usage: get_members [options]

Options:
        -g / --groupid=[groupid]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt
import operator

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGroups

def short_desc():
    return "Get a list of all owners in a given group."
        
def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hg:', ['help', 'groupid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupid = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            groupid = arg
    
    if not groupid:
        msg = "groupid is required."
        sys_exit(1,msg)
        
    # Create a authenticated Google service object 
    su_service = SuGroups()
        
    owners = su_service.RetrieveAllOwners(groupid)
    owners.sort(key=operator.itemgetter('email'))
    for o in owners:
        print o['email']

if __name__ == '__main__':
    main(*sys.argv[1:])
