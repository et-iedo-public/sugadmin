#! /usr/bin/python

"""Sync names with sunetids in OpenLdap.

This script synchronizes display name and family name changes with Stanford 
ldap directory.

Usage: sync-names [options]

Options:
        -u / --username=[username]
                Sync the user's names with central OpenLdap directory.
        -v / --verbose
                Show what's been done.
        -h / --help
                Print this message and exit

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit, remove_domain
from lib.Directory import *
from lib.SuService import SuUsers

def short_desc():
    return "Sync name changes with OpenLdap."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hvu:', ['help', 'verbose','username='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = '' 
    verbose = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username = remove_domain(arg)
        elif option in ('-v','--verbose'):
            verbose = True

    if not username:
        msg = "username is required."
        sys_exit(1,msg)

    if not is_valid_id(username):
        msg = username + " is not a valid userid."
        sys_exit(1,msg)

    su_service = SuUsers()
    if not su_service.is_ga_user(username):
        msg = username + "does not have a GA account."
        sys_exit(1,msg)


    attributes = [gdata_cfg.LASTNAME_FIRSTNAME]
    filter = 'uid=' + username
    name_lf = directory_query(filter,attributes,gdata_cfg.SEARCH_PATH_PPL)
    # Need to handle people just have one name, multiple names
    all_names=name_lf[0].split(',')
    if len(all_names) == 1:
        familyname = givenname = all_names[0]
    else:
        familyname,givenname=all_names[0].strip(),all_names[1].strip()

    # Get the current account values and update with new values
    user_entry = su_service.RetrieveUser(username)
    if familyname: 
        user_entry.name.family_name = familyname
    if  givenname:
        user_entry.name.given_name = givenname
        
    su_service.UpdateUser(username,user_entry)
    if verbose:
        print 'Familyname: %s, Givenname: %s' % ( user_entry.name.family_name, user_entry.name.given_name )

if __name__ == '__main__':
    main(*sys.argv[1:])
