#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University


""" Get all user account name in a domain.

Usage: get_orgs [options]
    -n / --name      Get orgunit based on its name
    -i / --id        Get customerid
    -A / -ALL        Print all orgs in the domain

"""

__author__ = 'sfeng@stanford.edu'

import getopt
import sys

import paths
from lib.Utils import sys_exit, str2bool
from lib.SuService import SuOrganization

def short_desc():
    return "Get organization units in the domain."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hiAn:', ['help','id','ALL', 'name='])
    except getopt.error, msg:
        sys_exit(1,msg)
        
    name = id = ''
    all = False
    
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0, __doc__)
        elif option in ('-n', '--name'):
            name = arg
        elif option in ('-A', '--ALL'):
            all = True
        elif option in ('-i', '--id'):
            id = True

    if name and all:
        msg = "Use either name or -A, not both."
        sys_exit(1, __doc__ + msg)

    su_service = SuOrganization()
    customerid = su_service.get_customer_id()
    if name:
        orgunit = su_service.RetrieveOrgUnit(customerid, name) 
        print orgunit
    elif id:
        print customerid
    else:
        orgs = su_service.RetrieveAllOrgUnits(customerid)
        for org in orgs:
            print 
            for k,v in org.iteritems():
                print '%-20s: %-20s' % (k,v)
         
if __name__ == '__main__':
    main(*sys.argv[1:])
