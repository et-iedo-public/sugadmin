#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University

"""
    get_delegate: get email delegates for a user.

Usage: get_delegate [options] 

Options:
        -u / --username=[username]
                Required. The delegator's username.
        -h / --help
                Print this message and exit.
    
Get email delegates for a given user.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email, add_domain, bool2str
from lib.SuService import SuEmail
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Get email delegates for a user."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:', ['help','username='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = '' 

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u', '--username'):
            username,udomain = parse_email(arg)

    if not username:
        msg = "Missing username."
        sys_exit(1, msg)
        
    if udomain and udomain.lower() != gdata_cfg.DOMAIN:
        msg = "delegator has to be in "+  gdata_cfg.DOMAIN + " domain."
        sys_exit(1,msg)

    su_service = SuEmail()
   
    gdfeed=su_service.RetrieveEmailDelegates(delegator=username)

    header_printed = False
    for entry in gdfeed:
        delegates = []
        keys = []
        for delegate in sorted(entry.iteritems(),key=operator.itemgetter(0)):
            k,v = delegate
            delegates.append(bool2str(v))
            keys.append(bool2str(k))
       
        if not header_printed:
            print ",".join(keys)
            header_printed = True
        print ",".join(delegates)

    #for entry in gdfeed:
    #    print entry

if __name__ == '__main__':
    main(*sys.argv[1:])
