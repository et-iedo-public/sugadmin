#!/usr/bin/python

"""Sync nickname with sunetids in OpenLdap.

A nickname is an alternative name that points to an existing primary user 
account. Email sent to the nickname address will be deliverd to the primary
user's mailbox.

This script sync nicknames with the primary user's sunetid aliases in 
Stanford ldap directory. This nickname exists in primary GA domain.

Usage: sync-nickname [options]

Options:
        -u / --username=[username]
                Sync the user's nicknames with central ldap's suSunetIDs.
        -v / --verbose
                Show what's been done.
        -h / --help
                Print this message and exit

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from lib.Utils import sys_exit, parse_email
from lib.Directory import directory_query
from lib.SuService import SuUsers

def short_desc():
    return "Sync nicknames with suSunetIDs in OpenLdap."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hvu:', ['help', 'verbose','username='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = '' 
    verbose = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username,domain = parse_email(arg)
        elif option in ('-v','--verbose'):
            verbose = True

    if not username:
        msg = "username is required."
        sys_exit(1,msg)

    su_service = SuUsers()

    if not SuUsers().is_ga_user(username):
        msg = username + "does not have a GA account."
        sys_exit(1,msg)

    # Sync nicknames with ldap. Delete existing nicknames first.
    nicknamefeed = su_service.RetrieveNicknames(username)
    for entry in nicknamefeed.entry:
        su_service.DeleteNickname(entry.nickname.name)
        if verbose:
            print "Deleted " + entry.nickname.name

    # Add nicknames (a.k.a. aliases) from ldap
    attributes = ['suSunetID']
    filter = 'uid=' + username
    new_nicknames = directory_query(filter,attributes,gdata_cfg.SEARCH_PATH_PPL)
    for nickname in new_nicknames:
        if nickname != username:
            su_service.CreateNickname(username,nickname)
            if verbose:
                print "Added " + nickname

if __name__ == '__main__':
    main(*sys.argv[1:])
