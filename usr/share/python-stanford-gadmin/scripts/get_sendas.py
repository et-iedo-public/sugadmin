#!/usr/bin/python

"""
get_sendas: Get "SendAs" addresses for a given user account.

Usage: get_sendas [options]

Options:
        -u / --username=[username]
            Username to get SendAs alias for.          
        -n / --noheader
            Do not print header for CSV output file (Username,Address,IsDefault,ReplyTo)                    
            Default is to print the header.
        -h / --help
            Print this message and exit.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator

import paths
import gdata_cfg
from  gdata.apps.service import AppsForYourDomainException
from lib.Utils import sys_exit, parse_email, bool2str
from lib.SuService import SuEmail

def short_desc():
    return "Get SendAs aliases for a user."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hnu:', ['noheader','help','username='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = ''
    header = True

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username,domain = parse_email(arg)
        if option in ('-n', '--noheader'):
            header = False

    if not username:
        msg = "username is required."
        sys_exit(1,msg)
 
    if domain and domain != gdata_cfg.DOMAIN:
        msg = "username needs to be in primary domain."
        sys_exit(1,msg)

    su_service = SuEmail()
    entries = su_service.RetrieveSendAsAliases(username) # list of dictionary entries

    if header:
        print "Username,Address,IsDefault,ReplyTo"
    for entry in entries:
        s = []
        for sendas in sorted(entry.iteritems(),key=operator.itemgetter(0)):
            k,v = sendas
            if k in ('sendAsId','name','verified'):
                continue
            s.append(bool2str(v))
         
        print username + "," + ",".join(s)

if __name__ == '__main__':
    main(*sys.argv[1:])
