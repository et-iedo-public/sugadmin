#!/usr/bin/python

"""
disable_user: Suspend an account. Messages will bounce and no login.

Usage: disable_user [options]

Options:
        -u / --username=[username]
        -h / --help
                Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import getopt
import sys

def short_desc():
    return "Suspend an account. Mail will bounce and login is locked."

import paths
from lib.Utils import sys_exit,parse_email
from lib.SuService import SuUsers

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:', ['help','username='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u', '--username'):
            username,domain = parse_email(arg)

    if not username:
        msg = "Username is required."
        sys_exit(0,msg)

    su_service = SuUsers()
    su_service.SuspendUser(username)

if __name__ == '__main__':
    main(*sys.argv[1:])
