#!/usr/bin/python
#
# Copyright 2012
#         The Board of Trustees of the Leland Stanford Junior University

"""get_resource: Get a Google Apps calendar resource information

Usage: delete_resource --resource "resourceid" | ALL

Options:
        -r / --resource=[resourceid]
                Required. Resource id to be deleted.
        -f / --force
                Do not check naming convention (e.g. cal_lo, cal_sc, cal_eq).
        -h / --help
                Print this message and exit
    
Delete a Google Apps calendar resource information. 

Example: 
    delete_resource --resource cal_lo_polya_hall_152 
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator

import paths
import gdata_cfg
from lib.Utils import sys_exit, friendly_domain_exception_exit
from lib.SuService import SuResource
from gdata.apps.service import AppsForYourDomainException

def short_desc():
    return "Delete a Google Apps calendar resource."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hfr:n:i:', ['help','force','resourceid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    resourceId =''
    force = False
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-r', '--resourceid'):
            resourceId = arg
        elif option in ('-f', '--force'):
            force = True

    if not resourceId:
        msg = "Resource id is required."
        sys_exit(1,msg)
    elif resourceId not in ('ALL') and not force:
        prefix = resourceId[:7] 
        try:
            gdata_cfg.RESOURCE_MAP[prefix]
        except KeyError:
            print "Wrong resource type. Should be one of the following:"
            print ', '.join(gdata_cfg.RESOURCE_MAP.keys())
            sys.exit(1)
        
    su_service = SuResource()

    if resourceId not in ('ALL'):
        su_service.DeleteResourceCalendar(resourceId.lower())
        print resourceId + " is deleted."
    else:
        all_resources = su_service.RetrieveAllResourceCalendars()
        all_resources.sort(key=operator.itemgetter('resourceId'))
        for r in all_resources:
            su_service.DeleteResourceCalendar(resourceId)
            print "%-30s: %s" % (resourceId, 'deleted')

if __name__ == '__main__':
    main(*sys.argv[1:])
