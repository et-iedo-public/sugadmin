#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

""" Command to allow IT staff to change mail routing

Usage: maildrop options

    -u / --user=[sunetid]
        The user's sunetid.
    -m / --mailbox=[mailbox]
        Optional. Set maildrop to a central mail server. 
    -v / --verbose
        Show detailed account's information in registry.
    -h / --help
        Print this doc.

If no mailbox is given, show the user's current maildrop setting. Otherwise, 
clear a specific domain (defined in /etc/gadmin/maildrop) forwarding and set 
the maildrop to the given value.

Example: maildrop -u sunetid -m zm88.stanford.edu

    Clear a subdomain forwarding if there is one and set maildrop to server 
    zm88 (Google Apps account). 

"""

__author__ = 'sfeng@stanford.edu'

import os
import socket
import re
import sys
import getopt
import remctl 
import ConfigParser

import paths
from lib.Directory import directory_query, is_valid_id, is_active, sys_exit
from lib.SuService import SuUsers

# Global configurations
hostname = re.sub('\d+','', socket.gethostname())
config = ConfigParser.SafeConfigParser()
config.read('/etc/gadmin/maildrop')
defaults = config.defaults()
LSDB = config.get(hostname,'lsdb')
ACCTSCRIPTS = config.get(hostname,'acctscripts')
DOMAIN = defaults['domain']
KRB5CCNAME = defaults['krb5ccname']
FORWARD_TO_CLEAR = defaults['forward_to_clear']
    
def short_desc():
    return "Verify or change maildrop."

def check_maildrop(user):
    """ Check current mail forward
    """
    cmd = ('hd','expn',user)
    try: 
        result = remctl.remctl(host='expn.stanford.edu',command=cmd)

        if result.stderr:
            sys.exit(result.stderr)
        elif result.stdout:
            return result.stdout
        else:
            sys.exit(user + " has no forward!")

    except remctl.RemctlProtocolError, error:
        sys.exit(error)

def reset_maildrop(user,maildrop,forwards):
    """ Reset maildrop
    Reset seas forwardings and set "seas local" service to the given 
    maildrop value
    """

    results = []
    if maildrop:
        # Update registry
        email = user + '@' + maildrop
        cmd = ('account','setting','--force',user, 'seas', 'local', email)
        try: 
            remctl.remctl(host=ACCTSCRIPTS,command=cmd)
        except remctl.RemctlProtocolError, error:
            sys.exit(error)

        # Update lsdb userdb
        cmd = ('user','maildrop',user,maildrop)
        try:
            result = remctl.remctl(host=LSDB,command=cmd)
        except remctl.RemctlProtocolError, error:
            sys.exit(error)

        results.append(result.stdout)

    if forwards:
        forwardvals = ' '.join(forwards)
    else: 
        forwardvals = ''

    cmd = ('account','setting',user, 'seas', 'forward', forwardvals)
    try: 
        result = remctl.remctl(host=ACCTSCRIPTS,command=cmd)
    except remctl.RemctlProtocolError, error:
       sys.exit(error)
    results.append(result.stdout)

    return '\n'.join(results)

def show_registry(user):
    cmd = ('account-show','show',user)
    try: 
        result = remctl.remctl(host=ACCTSCRIPTS,command=cmd)
        if result.stderr:
            sys.exit(result.stderr)
        elif result.stdout:
            print result.stdout
        else:
            sys.exit(user + " something is wrong!")
    except remctl.RemctlProtocolError, error:
        sys.exit(error)

def fully_qualify_host(host):
    dot = host.find('.')
    if dot < 0:
        host = host + '.' + DOMAIN

    return host

def forwards_to_keep(user):
    """
    Check current forwardings. Remove FORWARD_TO_CLEAR (e.g. gsb),
    return a list of other domain forwardings keep.
    """
    forwards = check_maildrop(user)
    if forwards:
        right_arrow = forwards.find('>')
        if right_arrow > 0:
            forwards = forwards[right_arrow+1:].split(',')

    forwards_to_keep = []
    for maildrop in forwards:
        maildrop = maildrop.rstrip()
        tmp_maildrop = maildrop.lower()
        if tmp_maildrop.find('@zm') > 0 or tmp_maildrop.find('@vacation') > 0:
            continue
        elif tmp_maildrop.find(FORWARD_TO_CLEAR) > 0:
            continue
        else:
            forwards_to_keep.append(maildrop)

    return forwards_to_keep

def main(*args):
    try:
        opts, args = getopt.getopt(args, 'hvu:m:',
                     ['help','verbose', 'user=', 'maildrop='])
    except getopt.error, msg:
        sys_exit(1, msg)

    user = maildrop = ''
    verbose = False

    for option, arg in opts:
        if option in ('-u', '--user'):
            user = arg.lower()
        elif option in ('-m', '--maildrop'):
            maildrop = arg.lower()
        elif option in ('-v', '--verbose'):
            verbose = True
        elif option in ('-h', '--help'):
            print __doc__
            sys.exit(0)

    if not user:
        sys.exit("User's sunetid is required.")

    if maildrop and maildrop.find('@') > 0:
        sys.exit("Maildrop should be a hostname, not email address.")

    if not is_valid_id(user):
        msg = user + " is not a valid sunetid."
        sys.exit(msg)

    if not is_active(user):
        msg = 'account ' + user + " is inactive."
        sys.exit(msg)

    if not SuUsers().is_ga_user(user):
        msg = user + " does not have a GA account."
        sys.exit(msg)

    os.environ['KRB5CCNAME'] = KRB5CCNAME

    forwards = forwards_to_keep(user)

    if maildrop:
        # Update maildrops
        maildrop = fully_qualify_host(maildrop)
        result = reset_maildrop(user,maildrop,forwards)
    else:
        result = check_maildrop(user)

    print result

    if verbose:
        show_registry(user)

if __name__ == '__main__':
    main(*sys.argv[1:])
