#!/usr/bin/python

"""
create_sendas: Create a Send-as alias to allow user to send from another 
address.

Usage: create_sendas [options]

Create a send as alias address to allow user to send from another address.

Options:
        -u / --username=[username]
                User to create alias for.          
        -n / --name=[display name]
                Display name of the alias. Default is user's display name set in StanfordYou.
        -a / --address=[email address]
                Email address to send from.
        -r / --replyto=[reply to address]
                Optional. Default is same as 'sendas'.
        -d / --default
                Optional. If set to true, the alias becomes the new default 
                alias to send-as for this user.
        -h / --help
                Print this message and exit.

"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
import gdata_cfg
from gdata.service import RanOutOfTries
from  gdata.apps.service import AppsForYourDomainException
from lib.Directory import is_valid_id, is_active
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuEmail,SuUsers,SuAlias, retry

def short_desc():
    return "Create a SendAs alias."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hdu:u:n:a:r:', ['help', 'default',
                     'username=','name=','alias=','replyto='])
    except getopt.error, msg:
        sys_exit(1, msg)

    username = alias = name = domain = subdomain = ''
    replyto = None
    makedefault = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        if option in ('-u', '--username'):
            username,domain = parse_email(arg)
        elif option in ('-a','--alias'):
            alias = arg
            alias,subdomain = parse_email(arg)
        elif option in ('-r','--replyto'):
            replyto = arg
        elif option in ('-d','--default'):
            makedefault = True

    if not ( username and alias ):
        msg = "username and alias is required."
        sys_exit(1,msg)

    if not is_valid_id(username):
        msg = username + ' is not a valid sunetid.'
        sys_exit(1,msg)
    elif not is_active(username):
        msg = username + ' is inactive.'
        sys_exit(1,msg)
    elif not SuUsers().is_ga_user(username):
        msg = username + ' does not have GA account.'
        sys_exit(1,msg)
 
    if domain and domain != gdata_cfg.DOMAIN:
        msg = "username needs to be in primary domain."
        sys_exit(1,msg)

    if subdomain.lower().find(gdata_cfg.SUBDOMAIN) < 0:
        msg = "alias address needs to be %s." % (gdata_cfg.SUBDOMAIN)
        sys_exit(1,msg)

    aliasemail = alias + '@' + subdomain
    if not replyto:
        replyto = aliasemail
  
    if not name:
        name = su_service = SuUsers()
        familyname = su_service.RetrieveUser(username).name.family_name  
        givenname = su_service.RetrieveUser(username).name.given_name
        name = givenname + " " + familyname
        
    su_service = SuAlias()
    useremail = username + '@' + gdata_cfg.DOMAIN
    if not su_service.is_alias(aliasemail):
        try:
            retry(su_service.CreateAlias,useremail,aliasemail,subdomain)
        except RanOutOfTries,e:
            raise  AppsForYourDomainException(e)
   
    su_service = SuEmail()
    try:
        retry(su_service.CreateSendAsAlias,username,name,aliasemail,replyto,makedefault)
    except RanOutOfTries,e:
            raise  AppsForYourDomainException(e)
        
    print "SendAs alias %s is created for %s. Default: %s" % (aliasemail, username, makedefault)

if __name__ == '__main__':
    main(*sys.argv[1:])
