#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""\nDelete a user's alias. 

Usage: delete_alias [options]

Options:
        -a / --email=[aliasemail]
                Alias email to delete. If email is not fully qualified, default to primary domain.
        -q / --quiet
                Quiet mode. Do not show status. Default is to show operation output. 
        -h / --help

    Delete a user alias. Deleted user alias can be used again as another alias.
    Caveat: If the alias is used as a SendAs address for a user, the SendAs cannot be deleted because Google API 
    doesn't support SendAs deletion yet. 

"""
__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

from gdata.apps.service import AppsForYourDomainException
from lib.Utils import sys_exit, parse_email
from lib.SuService import SuAlias
import gdata_cfg

def short_desc():
    return "Delete a user alias from the domain."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqa:', ['help', 'quiet', 'email='])
    except getopt.error, msg:
        sys_exit(1,msg)

    email = ''
    quiet = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-a', '--email'):
            email = arg
        elif option in ('-q', '--quiet'):
            quiet = True
            
    if not email:          
        msg = "User's alias is required."
        sys_exit(1,msg)
    
    if '@' not in email:
        email = email + '@' + gdata_cfg.DOMAIN
        
    domain = gdata_cfg.DOMAIN 
    
    su_service = SuAlias()
    su_service.DeleteAlias(email, domain)
    if not quiet:
        print "%s is deleted." % (email)

    # Comment out debugging code
    if False:
        try:
            result = su_service.DeleteAlias(email, domain)
        except AppsForYourDomainException, e:
            print "status: %s \nreason: %s \ngoogle reply: %s\n" % (e[0]['status'], e[0]['reason'], e[0]['body'])
        else:    
            print "user email: %s \nalias email: %s" % (result['userEmail'], result['aliasEmail'])

if __name__ == '__main__':
    main(*sys.argv[1:])
