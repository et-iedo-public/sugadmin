#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Add_owners: add one or more owners in the given group.

Usage: add_owners [options]

Options:
        -g / --groupid=[groupid]
        -h / --help
                Print this message and exit

        owner1 owner2 .. owners to be added to the group
"""

__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt
import time

from lib.SuService import SuGroups

def short_desc():
        return "Add owner(s) to a given group."
        
def main(*args):

    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hg:', ['help', 'groupid='])
    except getopt.error, msg:
        print msg
        sys.exit(1)

    groupid = ''
    owners = [x.lower() for x in args]

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            print __doc__
            sys.exit(0)
        elif option in ('-g', '--groupid'):
            groupid = arg

    if not groupid:
        print "Group id is required"
        sys.exit(1)
    if not owners:
        print "At least one owner is required"
        sys.exit(1)

    # Create a authenticated Google service object 
    su_service = SuGroups()
    su_service.RetrieveGroup(groupid)

    for owner in owners:
        su_service.AddOwnerToGroup(owner, groupid)
        time.sleep(1)

if __name__ == '__main__':
    main(*sys.argv[1:])
