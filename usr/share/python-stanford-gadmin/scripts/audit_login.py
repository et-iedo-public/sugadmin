#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

""" Audit account's login and other Google services information 

Usage: audit_account [options] [ <username> ]

Options:
    -c / --create 
         Create account information retrieval request

    -g / --get <requestid|ALL>
         Get the account information request status, or all requests's status.
         The staus can be either pending or complete. When in complete status,
         The output shows an file URL that we can download. The file is 
         encrypted with a public key pre-uploaded to Google. The private key
         is needed to decrypt the account information.

    -d / --delete <requestid|ALL>
         Delete a request or all requests

    -h / --help
         Print this message and exit
        
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt

import paths
from lib.Utils import sys_exit 
from lib.SuService import SuAudit
import gdata_cfg

def short_desc():
    return "Create login audit requests and manage these requests."

def main(*args):
  # Parse command line options
  try:
    opts, args = getopt.getopt(args, 'g:d:ch', ['help','create','get=','delete='])
  except getopt.error, msg:
    sys_exit(1,msg)

  action = None
  username = None
  requestid = None

  # Process options
  for option, arg in opts:
    if option in ('-h', '--help'):
      sys_exit(0,__doc__)
    elif option in ('-c','--create'):
      action = 'create'
    elif option in ('-g', '--get'):
      action = 'get'
      requestid = arg
    elif option in ('-d','--delete'):
      action = 'delete'
      requestid = arg

  if not action:
    sys_exit(0,__doc__)

  if len(args) <> 1 and requestid != 'ALL':
    msg = "Username is required."
    sys_exit(1,msg)
  elif requestid != 'ALL':
      username = args[0].lower().strip()

  su_service = SuAudit()
 
  if action == 'create':
      result = su_service.createAccountInformationRequest(username)
      for k,v in result.iteritems():
          print k, ": ", v 
  elif action == 'get':
      if requestid == 'ALL':
          result = su_service.getAllAccountInformationRequestsStatus()
          for entry in result:
              for k,v in entry.iteritems():
                  print k, ": ", v 
              print "\n"
       
      else:
          result = su_service.getAccountInformationRequestStatus(username,requestid)
          for k,v in result.iteritems():
              print k, ": ", v 
  else:
      result = su_service.deleteAccountInformationRequest(username, requestid)

if __name__ == '__main__':
  main(*sys.argv[1:])
