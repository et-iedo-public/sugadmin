#!/usr/bin/env python
#
# Copyright 2012
#     The Board of Trustees of the Leland Stanford Junior University

"""Manage calendar's access control list

Usage:  cal_acl_admin [options] <calendar> [share_with_address]

Manage <calendar> access rules. <calendar> can be a user's address or a calendar resource Id.

Options:
        -s / --show
            Show a calendar's access list.
        -g / --grant=[freebusy|read|editor|owner]
            Grant calendar access to user-address.
        -u / --update=[freebusy|read|editor|owner]
            Update calendar access list. 
        -r / --revoke
            Revoke access right from user-address 
        -h / --help
           Print this message and exit
Example:
    cal_acls_admin --show cal_lo_polya_hall_152
    cal_acls_admin --grant editor cal_lo_polya_hall_152 userid
    cal_acls_admin --update read cal_lo_polya_hall_152 userid
    cal_acls_admin --revoke calendar_lo_polya_hall_152 userid
"""
__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

from lib.Utils import add_domain,remove_domain
from lib.SuService import SuCalendar, SuResource
import gdata_cfg
import gdata.service
import gdata.calendar

ACL_URI_BASE = '/calendar/feeds/%s/acl/full'

def short_desc():
    return "Manage a user or resource calendar ACLs."


def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hsrg:u:', ['help', 'show', 'revoke','grant=', 'update='])
    except getopt.error, msg:
        print msg
        sys.exit(1)

    # Initialize variables
    user = cal = role =''
    grant = revoke = update = show = False
    addresses = [ add_domain(x.lower()) for x in args ]
    
    if not addresses:
        print "Calendar email address or user email address is required."
        print __doc__
        sys.exit(1)
    if not opts:
        print "Unknown options."
        sys.exit(1)
        
    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            print __doc__
            sys.exit(0)
        elif option in ('-u', '--update', '-g', '--grant'):
            role = arg
            if role not in ('freebusy', 'read', 'editor', 'owner'):
                print "acl should be one of the "'freebusy', 'read', 'editor', 'owner'""
                sys.exit(1)
            if option in ('-u', '--upgrade'):
                update = True
            else:
                grant = True         
        elif option in ('-r', '--revoke'):
            revoke = True
        elif option in ('-s', '--show'):
            show = True

    # At least calendar address is required
    if not addresses:
        print "Calendar address is required."
        sys.exit(1)
    else:
        cal = addresses[0]
    
    # If resouceId is given, find resource's email address
    prefix = cal[:7] 
    try:
        gdata_cfg.RESOURCE_MAP[prefix]
        resource_entry = SuResource().RetrieveResourceCalendar(remove_domain(cal))
        cal = resource_entry['resourceEmail']
    except KeyError:
        pass
    
    if not show and len(addresses) != 2:
        print "Calendar address and user address are required."
        sys.exit(1)
    elif not show:
        user = addresses[1]
                     
    if show:
        SuCalendar().show_acl(cal)
    elif grant:
        at_sign = user.find('@')
        if user[:at_sign] in ('domain','default'):
            print "The special user 'domain' and 'default' can\'t be added, please use update instead of add."
            sys.exit(1)
        SuCalendar().grant_acl(cal, user, role)
    elif revoke:
        SuCalendar().revoke_acl(cal, user)
    elif update:
        SuCalendar().update_acl(cal, user, role)
    else:
        SuCalendar().show_acl(cal)
    
    sys.exit(0)

if __name__ == '__main__':
    main(*sys.argv[1:])
