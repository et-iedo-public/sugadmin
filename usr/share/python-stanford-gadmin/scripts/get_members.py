#!/usr/bin/python

"""Get a list of all users in the given group.

Usage: get_members [options]

Options:
        -g / --groupaddr=[groupaddr]
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import getopt
import operator

import paths
from lib.Utils import sys_exit
from lib.SuService import SuGroups
import gdata_cfg

def short_desc():
    return "Get a list of all users in the given group."

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hg:', ['help','groupaddr='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupaddr = ''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupaddr'):
            groupaddr = arg
    
    if not groupaddr:
        msg = 'groupaddr is required.'
        sys_exit(1,msg)

    su_service = SuGroups()
    
    all_members = su_service.RetrieveAllMembers(groupaddr,True)
    all_members.sort(key=operator.itemgetter('memberId'))

    active_member_list = su_service.RetrieveAllMembers(groupaddr,False)
    getid = operator.itemgetter('memberId')
    active_member_ids = list(map(getid, active_member_list))

    for m in all_members:
        suspended = ''
        if m['memberId'] not in active_member_ids:
            suspended = '(suspended)'
        print m['memberId'] + suspended

if __name__ == '__main__':
    main(*sys.argv[1:])
