#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Add user(s) or group(s) to an existing Google Apps group.

Usage: add_members [options] <memberaddr1> <memberaddr2> ...

Options:
        -g / --groupaddr=[groupaddr]
        -q / --quiet 
                Quiet mode. No output
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt
import operator

from lib.Utils import sys_exit
from lib.SuService import SuGroups

def short_desc():
    return "Add user(s) or group(s) to an existing Google Apps group."
    
def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hqg:', ['help', 'quiet', 'groupid='])
    except getopt.error, msg:
        sys_exit(1,msg)

    groupid = ''
    quiet = False
    members = [x.lower() for x in args]

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-g', '--groupid'):
            groupid = arg
        elif option in ('-q', '--quiet'):
            quiet = True

    if not (groupid and members):
        msg = 'groupid and member(s) are required.'
        sys_exit(1,msg)

    # Create a authenticated Google service object 
    su_service = SuGroups()
    
    #Call su service to get data
    su_service.RetrieveGroup(groupid)
    
    for memberid in members:
        # We do not check if memberaid is a valid Google account or not,
        # because members can be from other domains.
        # We also skip non-fully qualified domain name
        if memberid.find('@') > 0:
            su_service.AddMemberToGroup(memberid,groupid)
        else:
            print "Skip non fully qualfied memberid: " + memberid

    if not quiet:
        members = su_service.RetrieveAllMembers(groupid, True)
        members.sort(key=operator.itemgetter('memberId'))
        for m in members:
            print m['memberId']

if __name__ == '__main__':
    main(*sys.argv[1:])
