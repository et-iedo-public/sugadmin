#!/usr/bin/python

import os
import sys

# Change this to your installation path of gadmin
prod = '/usr/lib/gadmin'
dev  = os.path.abspath(__file__)
dev = os.path.dirname(dev)   #script
dev = os.path.dirname(dev)   #gadmin
# print "DEV dir: " + dev
gdatalib = dev + '/gdata-lib/lib/python'
scripts = dev + '/scripts'
testdir = dev + '/test'
gdatacfg = '/etc/gadmin/'

# Paths to search for modules
sys.path.insert(0, testdir)
sys.path.insert(0, dev)
sys.path.insert(0, gdatalib)
sys.path.insert(0, scripts)
sys.path.insert(0, gdatacfg)
sys.path.insert(0, prod)
