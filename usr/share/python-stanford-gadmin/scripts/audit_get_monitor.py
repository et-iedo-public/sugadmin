#!/usr/bin/python
#
# Copyright 2011
#     The Board of Trustees of the Leland Stanford Junior University

"""Get the email monitor for the given user

Usage: audit_get_monitor [options] <username>

Options:
    -u / --username 
        Get the user's email monitor information
    -h / --help
        Print this message and exit

    Example: audit-get-monitor sfeng
        Get monitor status for sfeng
"""

__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

import paths
from lib.Utils import sys_exit,parse_email
from lib.SuService import SuAudit
import gdata_cfg

def short_desc():
    return "Get email monitor information for a user."

def main(*args):
  # Parse command line options
  try:
    opts, args = getopt.getopt(args, 'hu:', ['help','--username='])
  except getopt.error, msg:
    sys_exit(1,msg)

  username = ''
  # Process options
  for option, arg in opts:
    if option in ('-u','--username'):
        username,domain = parse_email(arg)
    if option in ('-h', '--help'):
      sys_exit(0,__doc__)

  if not username:
    msg = "Username is required."
    sys_exit(1,msg)

  su_service = SuAudit()
 
  result = su_service.getEmailMonitors(username)
  for r in result:
    for k,v in r.iteritems():
        print k, ":", v
    print "\n"

if __name__ == '__main__':
  main(*sys.argv[1:])
