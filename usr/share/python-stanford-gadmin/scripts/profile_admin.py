#!/usr/bin/env python
#
# Copyright 2012
#     The Board of Trustees of the Leland Stanford Junior University

"""Manage user profile.

Usage:  profile_admin [options] userid ...

Options:
        -i / --info
        -s / --share=[Yes|No]
        -q / --quiet
        -h / --help
           Print this message and exit
"""
__author__ = 'sfeng@stanford.edu'

import sys
import paths
import getopt

from lib.Utils import parse_email, str2bool, bool2str
from lib.SuService import SuContacts
import gdata_cfg
import gdata.client

def short_desc():
    return "Get a user's account profile."

def get_info(username,profile_service):
    """Retrieve user profile information
    """
    # uri = 'https://www.google.com/m8/feeds/profiles/domain/%s/full/%s?v=3.0' % (domain, username)
    entry_uri = profile_service.GetFeedUri('profiles')+'/'+username + '?v=3.0'
    try:
        user_profile = profile_service.GetProfile(entry_uri)
    except gdata.client.RequestError, e:
        print 'Error: %s@%s %s' % (username, gdata_cfg.DOMAIN, e[0])
        sys,exit(1)

    for email in user_profile.email:
        if email.primary == 'true':
            print 'Email: %s (primary)' % (email.address)
        else:
            print 'Email: %s' % (email.address)
        if user_profile.nickname:
            print 'Nickname: %s' % (user_profile.nickname.text)

    indexed = user_profile.extension_elements[2].attributes['indexed']
    if indexed == 'true':
        print "Profile Shared: Yes\n"
    else:
        print "Profile Shared: No\n"
    
def set_share(username, indexed, profile_service, quiet):
    """ Share or unshare user's profile
    """
    
    entry_uri = profile_service.GetFeedUri('profiles')+'/'+username + '?v=3.0'
    try:
        user_profile = profile_service.GetProfile(entry_uri)
        user_profile.extension_elements[2].attributes['indexed'] = indexed
        profile_service.UpdateProfile(user_profile.GetEditLink().href, user_profile)
        if not quiet:
            print 'Set Profile Sharing to %s for %s@%s.' % (indexed, username,gdata_cfg.DOMAIN)
    except gdata.service.RequestError, e:
        print 'Error: %s@%s %s - %s' % (username, gdata_cfg.DOMAIN, e[0]['body'], e[0]['reason'])
        sys.exit(1)
        
def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hiqs:', ['help', 'info', 'quiet','share='])
    except getopt.error, msg:
        print msg
        sys.exit(1)

    user_ids = [x.lower() for x in args]

    share = ''
    quiet = info = False

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            print __doc__
            sys.exit(0)
        elif option in ('-u', '--username'):
            username,domain=parse_email(arg)
        elif option in ('-i', '--info'):
            info = True
        elif option in ('-s', '--share'):
            share = arg.lower()
        elif option in ('-q', '--quiet'):
            quiet = True

    if not user_ids:
        print 'At least one userid is required.'
        sys.exit(1)
    if (not info and not share) or (info and share):
        print __doc__
        sys.exit(0)
    if share and share not in ('yes', 'no'): 
        print 'share has to be "yes" or "no".'
        sys.exit(1)
    else:
        # indexed has to be a string, 'true', 'false'
        indexed = bool2str(str2bool(share)).lower()

    # Get profile object
    profile_service = SuContacts()
    for id in user_ids:
        if info:
            get_info(id,profile_service)    
        else:
            set_share(id, indexed, profile_service, quiet)
    
    sys.exit(0)

if __name__ == '__main__':
    main(*sys.argv[1:])
