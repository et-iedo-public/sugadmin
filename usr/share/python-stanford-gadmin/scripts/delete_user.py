#!/usr/bin/python
#
# Copyright 2011
#         The Board of Trustees of the Leland Stanford Junior University


"""delete_user: Delete a user. Account name cannot be re-used in 5 days.

Usage: delete_user [options]

Options:
        -u / --username=[username]
                Account name to be deleted.
        -f / --file=[file]
                A file that includes accounts to be deleted.
        -h / --help
                Print this message and exit
"""

__author__ = 'sfeng@stanford.edu'

import getopt
import sys
import time

import paths
import gdata_cfg
from gdata.service import DEFAULT_NUM_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF, RanOutOfTries
from lib.Utils import sys_exit, parse_email, readlines
from lib.SuService import SuUsers
from gdata.apps.service import AppsForYourDomainException


def short_desc():
    return "Delete a user account."

def delete_account(user):
    """Delete user account. When an account is deleted, nickname goes away too.
    """
    su_service = SuUsers()
    
    # Retry if we got 503 service unavailable due to ratelimit
    mtries, mdelay = DEFAULT_NUM_RETRIES, DEFAULT_DELAY
    while mtries > 0:
        try:
            su_service.DeleteUser(user)
            print "%s is deleted." % (user)
            break
        except AppsForYourDomainException, e:
            if e.args[0]['status'] == 503:
                mtries -= 1
                mdelay *= DEFAULT_BACKOFF
                time.sleep(mdelay)
            else:
                raise AppsForYourDomainException, e

    if mtries == 0:
        raise RanOutOfTries('Ran out of tries.')

def main(*args):
    # Parse command line options
    try:
        opts, args = getopt.getopt(args, 'hu:f:', ['username=', 'file='])
    except getopt.error, msg:
        sys_exit(1,msg)

    username = file =''

    # Process options
    for option, arg in opts:
        if option in ('-h', '--help'):
            sys_exit(0,__doc__)
        elif option in ('-u','--username'):
            username,domain = parse_email(arg)
        elif option in ('-f','--file'):
            file = arg
    
    if username and file:
        msg = "Use either username or file, not both."
        sys_exit(1,msg)

    if not username and not file:
        msg = "username or file is required."
        sys_exit(1,msg)
        
    accounts = []
    rc = 0
    if username:
        accounts.append(username)
    else:
        lines = readlines(file)
        for l in lines:
            accounts.append(l.rstrip())
            
    for user in accounts:
        try:
            delete_account(user)
        except AppsForYourDomainException, e:
            if e.args[0]['body'].rfind('EntityDoesNotExist') > 0:
                print "%s does not exist." % (user)
                rc = 1

        password = ''

    sys.exit(rc)
    
if __name__ == '__main__':
    main(*sys.argv[1:])
