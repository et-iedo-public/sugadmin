# -*- python -*-

# Copyright (C) 2011, Stanford University

"""Utility modules""" 

import os
import sys
import re
from hashlib import sha1
from random import random

import paths
import gdata_cfg

def generate_password():
    return sha1(str(random())).hexdigest()

def sys_exit(code, msg=''):
    if code:
            fd = sys.stderr
    else:
            fd = sys.stdout
    if msg:
            print >> fd, msg
    sys.exit(code)

def return_status(code, msg=''):
    if code:
        fd = sys.stderr
    else:
        fd = sys.stdout
    if msg:
        print >> fd, msg

    return code

def str2bool(v):
    """convert string 'yes' 'no' to boolean."""
    return v.lower() in ["yes", "true"]

def bool2str(v):
    """convert boolean to string."""
    if type(v) is bool:
        if v:
            return 'True'
        else:
            return 'False'
    elif v is None:
            return 'False'

    return v

def writefile(filename,line):
    """ Write to a file."""
    try:
        fp = open(filename, 'w')
        fp.write(line)
        fp.close()
    except IOError, e:
        sys_exit(2, e)
        
def readfile(filename):
    """Read from a file and return its content."""
    if filename == '-':
        fp = sys.stdin
        closep = 0
    else:
        fp = open(filename)
        closep = 1

    content = fp.read()
    if closep:
        fp.close()
    return content

def readlines(filename):
    """Read from a file and return list of lines."""
    if filename == '-':
        fp = sys.stdin
        closep = 0
    else:
        try:
            fp = open(filename)
            closep = 1
        except IOError, e:
            sys_exit(2,e)

    lines = fp.readlines()
    if closep:
        fp.close()
    return lines

def add_domain(addr):
    """
    Take an address and add default domain if the address is not fully 
    qualified.
    """
    addr = addr.lower()
    at_sign = addr.find('@')
    if at_sign < 1:
        addr  = addr + '@' + gdata_cfg.DOMAIN

    return addr

def remove_domain(addr):
    """
    Take an address and remove domain if it exists. Return userid.
    """
    addr = addr.lower()
    at_sign = addr.find('@')
    if at_sign < 1:
        return addr
    else:
        return addr[:at_sign]

def parse_email(addr):
    """
    Takes an email address, and returns a tuple containing (user,host).

    """
    at_sign = addr.find('@')
    if at_sign < 1:
        return addr, gdata_cfg.DOMAIN
    
    return addr[:at_sign], addr[at_sign+1:]

def friendly_domain_exception_exit(e,exit=False):
    """
    Process gdata.apps.service.AppsForYourDomainException message
    If exit is True, exit the code with error 2. Otherwise, print message and pass

    """
    try:
        msg = e.reason
    except:
        msg = e.args[0]['reason']
        try:
            reason = re.search('reason="(.*)" ', e.args[0]['body'])
            msg = msg + reason.group(1)
        except:
            pass

    if exit:
        sys_exit(2, msg)
    else:
        return msg
