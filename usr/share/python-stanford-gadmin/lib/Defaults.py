# -*- python -*-

# Copyright (C) 2011, Stanford University

# Default ldap serverh, search path, and search attribute
LDAP_SERVER      = 'ldap.stanford.edu'
SEARCH_PATH_ACCT = 'cn=accounts,dc=stanford,dc=edu'
SEARCH_PATH_PPL  = 'cn=people,dc=stanford,dc=edu'

# Kerberos principal for ldap lookup
MAIL_KRB5CCNAME = '/var/run/pobox.k5.tgt'

# Supported subdomains
SUBDOMAINS = [ 'stanford.edu',
               'demo.itlab.stanford.edu',
               'subdemo.itlab.stanford.edu',
               'test.itlab.stanford.edu',
               'gsb.stanford.edu',
    ]
