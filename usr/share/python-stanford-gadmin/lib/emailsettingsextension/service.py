# -*- python -*-

# Copyright  2011
#     The Board of Trustees of the Leland Stanford Junior University

'''
Created on May 22, 2011

@author: sfeng@stanford.edu
'''

__author__ = 'sfeng@stanford.edu'

from gdata.apps.emailsettings.service import EmailSettingsService

class EmailSettingsServiceExt(EmailSettingsService):
    """EmailSettings extension for the EmailSettings service."""
    
    def RetrieveSendAsAliases(self,username):
        """Retrieve all alias for a user.

        Args:
            username: User to retrieve alias for.

        Returns:
          A dict containing the result of the SendAs aliases
        """
        uri = self._serviceUrl('sendas', username)
        return self._GetPropertiesList(uri)

    def DeleteSendAsAlias(self,username,sendas):
        """Retrieve all alias for a user.

        Args:
            username: User to retrieve alias for.

        Here is no API to do this! Place holder in case Google has one

        """
        properties = {}
        properties['name'] = username
        properties['address'] = sendas
        uri = self._serviceUrl('sendas', username)
        print uri
        return self._DeleteProperties(uri)

    def AddEmailDelegate(self, delegate, delegator):
        """Create delegate

        Args:
            delegate: User who will have access to delegator's account
            delegator: User whose account will be accessible by delegate

        Returns:
            A dict containing the result of the operation.
        """
        uri = self._serviceUrl('delegation', delegator)
        properties = {'address': delegate}
        return self._PostProperties(uri, properties)

    def RetrieveEmailDelegates(self, delegator):
        """Retrieve delegates

        Args:
          delegator: User whose account is accessible by retrieved delegates

        Returns:
          A dict contaning the delegates
        """
        uri = self._serviceUrl('delegation', delegator)
        return self._GetPropertiesList(uri)


    def DeleteEmailDelegate(self, delegate, delegator):
        """Delete delegate

        Args:
          delegate: User account who has access to delegator's account
          delegator: Email address whose account will no longer be accessible by delegate

        Returns:
        A dict containing the result of the operation.
        """
        uri = self._serviceUrl('delegation', delegator)+"/%s" % delegate
        return self._DeleteProperties(uri)
