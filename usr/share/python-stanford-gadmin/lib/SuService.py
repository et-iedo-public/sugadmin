# -*- python -*-

# Copyright  2011, 2012
#     The Board of Trustees of the Leland Stanford Junior University

'''
Created on May 22, 2011

@author: sfeng@stanford.edu
'''

import os.path, time, sys
import paths
import gdata_cfg
import gdata.auth
import gdata.calendar
import pickle # Store token

from gdata.service import DEFAULT_NUM_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF, RanOutOfTries
from gdata.apps.service import AppsForYourDomainException
from gdata.apps.service import AppsService
from gdata.apps.groups.service import GroupsService
from gdata.apps.adminsettings.service import AdminSettingsService
from gdata.apps.audit.service import AuditService
from gdata.apps.organization.service import OrganizationService 
#from gdata.apps.emailsettings.client import EmailSettingsClient
from gdata.apps.emailsettings.service import EmailSettingsService

from gdata.docs.service import DocsService
from gdata.spreadsheet.service import SpreadsheetsService
from gdata.contacts.service import ContactsService
from gdata.calendar.service import CalendarService

from lib.calendar_resource.service import ResCalService
from lib.alias.service import AliasService
from lib.Utils import readfile, writefile
from lib.emailsettingsextension.service import EmailSettingsServiceExt

from gdata.docs.service import DocsService
from gdata.spreadsheet.service import SpreadsheetsService
from gdata.contacts.service import ContactsService

ACL_URI_BASE = ACL_URI_BASE = '/calendar/feeds/%s/acl/full'

# OAuth token
class DoOAuth():
    def __init__(self):
        self.domain = gdata_cfg.DOMAIN
        self.source = gdata_cfg.OAUTH_TOKEN_NAME['xoauth_displayname']
        token = self.tryOAuthToken()
        self.SetOAuthInputParameters(gdata.auth.OAuthSignatureMethod.HMAC_SHA1, 
            consumer_key=token.oauth_input_params._consumer.key, 
            consumer_secret=token.oauth_input_params._consumer.secret)
        token.oauth_input_params = self._oauth_input_params
        self.SetOAuthToken(token)
   
    def tryOAuthToken(self):
        """Read token from token file
        """
        msg = ''
        if os.path.exists(gdata_cfg.OAUTH_TOKEN_FILE):
            try:
                oauthfile = open(gdata_cfg.OAUTH_TOKEN_FILE, 'rb')
                domain = oauthfile.readline()[0:-1]
                token = pickle.load(oauthfile)
                oauthfile.close()
                f = open(gdata_cfg.OAUTH_TOKEN_FILE, 'wb')
                f.write('%s\n' % (domain,))       
                pickle.dump(token, f)
                f.close()
                if not token:
                    raise EOFError("Empty token file")
            except EOFError:
                msg = "OAuth token file invalid. Please run oauth_admin to create oauth token."
        else:
            msg = "No OAuth token file. Please run oauth_admin to create the access token."
        
        if msg:
            print msg
            sys.exit(1)
            
        return token
    
# Client login token
class DoClientLogin(AppsService):
    def __init__(self):
        """Constructor for the service object.
        Subclass GroupService and AppsService
        """   
        AppsService.__init__(self)

        self.domain = gdata_cfg.DOMAIN       
        self.email = gdata_cfg.SITEADMIN
        self.password = gdata_cfg.PASSWD
        self.source = gdata_cfg.OAUTH_TOKEN_NAME['xoauth_displayname']
        self.setAuthToken()

    def setAuthToken(self):
        """ Read token from token file
        """
        if os.path.exists(gdata_cfg.AUTH_TOKEN):
            try:
                if time.time() - os.path.getmtime(gdata_cfg.AUTH_TOKEN) > (24 * 60 * 60 ) - 5:    
                    raise IOError("Expired token file")    

                token = readfile(gdata_cfg.AUTH_TOKEN)
                if not token:  # or auth token file older then 24h
                    raise IOError("Empty token file")
            except IOError:
                token = self.getAuthToken()
        else:
            token = self.getAuthToken()
            
        # TODO: insert the token into the token_store directly.
        self.SetClientLoginToken(token)
        self.__captcha_token = None
        self.__captcha_url = None
        print "TOKEN" + token
        return token
            
    def getAuthToken(self):
        self.ProgrammaticLogin()
        token = self.GetClientLoginToken()
        print "Got new token."
        writefile(gdata_cfg.AUTH_TOKEN,token)
        return token
      
class SuCalendar(DoOAuth,CalendarService):
    def __init__(self):
        CalendarService.__init__(self)   
        CalendarService.ssl = True
        DoOAuth.__init__(self)  #has to be the last to init to set all authn 

    def grant_acl(self, calendar, user, role):
        """ Grant user access to calendar
        """    
        rule = gdata.calendar.CalendarAclEntry()
        rule.scope = gdata.calendar.Scope(value=user)
        rule.scope.type = 'user'
        roleValue = 'http://schemas.google.com/gCal/2005#%s' % (role)
        rule.role = gdata.calendar.Role(value=roleValue)
        acl_uri = ACL_URI_BASE % calendar

        try:
            print "Giving %s %s access to calendar %s" % (user, role, calendar)
            returned_rule = self.InsertAclEntry(rule, acl_uri)
        except gdata.service.RequestError, e:
            print 'Error: %s - %s' % (e[0]['reason'], e[0]['body'])
            sys.exit(e[0]['status'])

        return True
    
    def show_acl(self,calendar):
        """Retrieve calendar acl
        """
        acl_uri = ACL_URI_BASE % calendar
        try:
            cal_acl = self.GetCalendarAclFeed(acl_uri)
        except gdata.service.RequestError, e:
            print 'Error: %s - %s' % (e[0]['reason'], e[0]['body'])
            sys,exit(1)
     
        aclcount = 0
        print cal_acl.title.text
        for aclcount, rule in enumerate(cal_acl.entry):
            print '  Scope %s - %s' % (rule.scope.type, rule.scope.value)
            print '  Role: %s' % (rule.title.text)
            print ''
        
        if aclcount:
            print "Total of %s acl entries on %s." % (aclcount+1, cal_acl.title.text)
            
        return True

    def revoke_acl(self,calendar, user):
        """ Rovoke user access to calendar
        """
        acl_uri = ACL_URI_BASE % calendar
        try:
            feed = self.GetCalendarAclFeed(uri=acl_uri)
        except gdata.service.RequestError, e:
            print 'Error: %s - %s' % (e[0]['reason'], e[0]['body'])
            sys.exit(e[0]['status'])
        found_rule = False
        print "Removing %s's access to calendar %s" % (user, calendar)
        for i, a_rule in enumerate(feed.entry):      
            try:     
                if (user == 'default' and a_rule.scope.value == None) or \
                    (a_rule.scope.type.lower() == 'domain' and user == 'domain') or a_rule.scope.value.lower() == user:
                    found_rule = True
                    try:
                        result = self.DeleteAclEntry(a_rule.GetEditLink().href)
                    except gdata.service.RequestError, e:
                        print 'Error: %s - %s' % (e[0]['reason'], e[0]['body'])
                        sys.exit(e[0]['status'])
                    break
            except AttributeError:
                continue
        if not found_rule:
            print 'Error: that object does not seem to have access to that calendar'
            sys.exit(1)
    
    def update_acl(self,calendar, user, role):
        """ Update user's access right to calendar
        """
        rule = gdata.calendar.CalendarAclEntry()
        if user == 'domain':
            ruleValue = self.domain
            ruleType = 'domain'
        elif user == 'default':
            ruleValue = None
            ruleType = 'default'
        else:
            ruleValue = user
            ruleType = 'user'
            
        rule.scope = gdata.calendar.Scope(value=ruleValue)  
        rule.scope.type = ruleType
        roleValueUrl = 'http://schemas.google.com/gCal/2005#%s' % (role)
        rule.role = gdata.calendar.Role(value=roleValueUrl)
        if ruleType != 'default':
            acl_uri = '/calendar/feeds/%s/acl/full/%s%%3A%s' % (calendar, ruleType, ruleValue)
        else:
            acl_uri = '/calendar/feeds/%s/acl/full/default' % (calendar)
            
        try:
            self.UpdateAclEntry(edit_uri=acl_uri, updated_rule=rule)
        except gdata.service.RequestError, e:
            print 'Error: %s - %s' % (e[0]['reason'], e[0]['body'])
            sys.exit(e[0]['status'])
  
    
class SuContacts(DoOAuth,ContactsService):
    def __init__(self):
        ContactsService.__init__(self,contact_list=gdata_cfg.DOMAIN)   
        ContactsService.ssl = True
        DoOAuth.__init__(self)  #has to be the last to init to set all authn 
    
class SuAudit(DoOAuth, AuditService):
    def __init__(self):
        AuditService.__init__(self)   # select the service only needed for this service.
        DoOAuth.__init__(self)  #has to be the last to init to set all authn 

class SuGroups(DoOAuth,GroupsService):
    def __init__(self):    # select the service only needed for this service.
        GroupsService.__init__(self) #has to be the last to init to set all authn 
        DoOAuth.__init__(self)
    
class SuResource(DoOAuth,ResCalService):
    def __init__(self):
        ResCalService.__init__(self,domain = gdata_cfg.DOMAIN)
        DoOAuth.__init__(self)
       
class SuUsers(DoOAuth, AppsService):
    def __init__(self):
        AppsService.__init__(self)
        DoOAuth.__init__(self)

    def is_ga_user(self,user):
        try:
            self.RetrieveUser(user)
        except:
            return False

        return True

class SuAdmin(DoOAuth, AdminSettingsService):
    def __init__(self):
        AdminSettingsService.__init__(self)   # select the service only needed for this service.
        DoOAuth.__init__(self)  #has to be the last to init to set all auth
        
class SuAlias(DoOAuth,AliasService):
    def __init__(self):
        AliasService.__init__(self)   # select the service only needed for this service.
        DoOAuth.__init__(self)  #has to be the last to init to set all auth

    def is_alias(self,alias):
        try:
            self.RetrieveAlias(alias,gdata_cfg.SUBDOMAIN)
        except:
            return False

        return True
 
class SuEmail(DoOAuth, EmailSettingsServiceExt):
    def __init__(self):
        EmailSettingsServiceExt.__init__(self)   # select the service only needed for this service.
        DoOAuth.__init__(self)  #has to be the last to init to set all authn 

class SuDocs(DoOAuth, DocsService):
    def __init__(self):
        DocsService.__init__(self)   # select the service only needed for this service.
        DoOAuth.__init__(self)  #has to be the last to init to set all authn

class SuOrganization(DoOAuth, OrganizationService):
    def __init__(self):
        OrganizationService.__init__(self)  # select the service only needed for this service.
        DoOAuth.__init__(self)  # has to be the last to init to set all authn 

    def get_customer_id(self):
        try:
            customerid = self.RetrieveCustomerId()
            customerid = customerid['customerId']
        except:
            return False

        return customerid

  
    def get_user_cos(self,user):
        """Get a user to current org
        """
        customerid = self.get_customer_id()
        return self.RetrieveOrgUser(customerid,user)

    def move_to_cos(self,user,cos):
        """Move a user to another class of service group
        """
        customerid = self.get_customer_id()
        delay = 0
        while True:
            try:
                self.UpdateOrgUser(customerid,user,cos)
                break
            except AppsForYourDomainException, e:
                if e.args[0]['status'] == 503:
                    delay += gdata_cfg.DELAY_INCREMENT
                    time.sleep(delay)
                else:
                    raise AppsForYourDomainException, e
        return True

def retry(func, *args):

    """ Retries an arbitrary function and then raises RanOutOfTries on
    on failure.  Always raises keyboard interrupt.
    """

    mtries, mdelay = DEFAULT_NUM_RETRIES, DEFAULT_DELAY
    while mtries > 0:
        try:
            result = func(*args)
        except KeyboardInterrupt:
            raise
        except Exception, e:
            print e
            mtries -= 1
            mdelay *= DEFAULT_BACKOFF
            time.sleep(mdelay)
        else:
            return result 

    raise RanOutOfTries('Ran out of tries.')
        
if False:       
    
    # Example of subclass of GA's multiple classes, if doing it in one class
    class SuGoogleService(AppsService,GroupsService,AdminSettingsService,AuditService):
        def __init__(self):
            """Constructor for the object.
            Subclass GroupService and AppsService
            """
            # Run parent classes to initialize variable
            GroupsService.__init__(self)
            AdminSettingsService.__init__(self)
            AuditService.__init__(self)
            AppsService.__init__(self) # This has to be the last one
        
            self.domain = gdata_cfg.DOMAIN       
            self.email = gdata_cfg.SITEADMIN
            self.password = gdata_cfg.PASSWD
            self.source = gdata_cfg.OAUTH_TOKEN_NAME['xoauth_displayname']
            self.ProgrammaticLogin()
