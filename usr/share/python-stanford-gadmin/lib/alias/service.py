# -*- python -*-

# Copyright  2011
#     The Board of Trustees of the Leland Stanford Junior University

'''
Created on May 22, 2011

@author: sfeng@stanford.edu
'''

__author__ = 'sfeng@stanford.edu'


import urllib
import gdata.apps
import gdata.apps.service
import gdata.service

API_VER = '2.0'

class AliasService(gdata.apps.service.PropertyService):
    """Client for the Google Apps Alias service."""

    def _baseUrl(self, domain=None):
        if domain is None:
            domain = self.domain
        return '/a/feeds/alias/%s/%s' % (API_VER, domain)

    def RetrieveAlias(self, alias, domain):
        """Retrieve the user email of given alias in domain

        Args:
          alias: The alias of the user
          domain: The name of the domain.
        
        Returns:
          The dict with userEmail and aliasEmail.
        """
        base_uri = self._baseUrl(domain)
        service_uri = '%s/%s' % (base_uri, alias)
        result =  self._GetProperties(service_uri)
        return result
    
    def CreateAlias(self, email, alias, domain):
        """Create a user alias in a domain

        Args:
          email: The user email
          alias: The alias email
          domain: The domain in which the alias will be created.
        
        Returns:
          The dict with userEmail and aliasEmail.
        """
        service_uri = self._baseUrl(domain)
        properties = {}
        properties['userEmail'] = email
        properties['aliasEmail'] = alias
        result = self._PostProperties(service_uri, properties)
        return result

    def DeleteAlias(self, alias, domain):
        """Delete the user email of given alias in domain

        Args:
          alias: The alias of the user
          domain: The name of the domain.
        
        """
        base_uri = self._baseUrl(domain)
        service_uri = '%s/%s' % (base_uri, alias)
        try:
            self._DeleteProperties(service_uri)
        except gdata.service.RequestError, e:
          raise AppsForYourDomainException(e.args[0])
